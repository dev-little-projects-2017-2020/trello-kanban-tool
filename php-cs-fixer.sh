#!/usr/bin/env bash
docker-compose run --rm php-cs-fixer fix src
docker-compose run --rm php-cs-fixer fix tests