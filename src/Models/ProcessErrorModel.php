<?php

namespace Infomaniak\TrelloKanban\Models;

/**
 * Class ProcessError
 *
 * @package Infomaniak\TrelloKanban\KanbanModels
 */
class ProcessErrorModel
{
    public $id;
    public $boardId;
    public $date;
    public $resolved;
    public $type;
    public $listId;
    public $cardId;
    public $description;
}
