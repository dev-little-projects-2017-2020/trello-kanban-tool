<?php

namespace Infomaniak\TrelloKanban\Models;

use Infomaniak\TrelloKanban\Features\KanbanCard;

/**
 * Class CardModel
 *
 * @package Infomaniak\TrelloKanban\KanbanModels
 */
class CardModel
{
    public $id;
    public $cardTrelloId;
    public $listId;
    public $description;
    public $dueAt;
    public $completedAT;
    public $type;
    public $name;
    public $link;

    /**
     * @param array $trello
     * @param       $listId
     *
     * @return CardModel
     */
    public static function retrieveFromTrello(array $trello, $listId): CardModel
    {
        $model               = new self();
        $model->id           = null;
        $model->cardTrelloId = $trello['id'];
        $model->listId       = $listId;
        $model->description  = $trello['desc'];
        $model->dueAt        = empty($trello['due']) ? null : substr($trello['due'], 0, 10);
        $model->completedAT  = null;
        $model->type         = KanbanCard::MAINTENANCE;
        foreach ($trello['labels'] as $label) {
            $model->type = isset(KanbanCard::$typeByLabel[$label['name']]) ? KanbanCard::$typeByLabel[$label['name']] : $model->type;
        }
        $model->name = $trello['name'];
        $model->link = $trello['shortUrl'];

        return $model;
    }
}
