<?php

namespace Infomaniak\TrelloKanban\Models;

/**
 * Class KanbanModel
 *
 * @package Infomaniak\TrelloKanban\KanbanModels
 */
abstract class KanbanModel implements \JsonSerializable
{

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        $reflex = new \ReflectionClass($this);
        $array  = [];
        foreach ($reflex->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
            $array[$property->name] = $property->getValue($this);
        }
        return $array;
    }
}
