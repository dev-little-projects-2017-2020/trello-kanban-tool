<?php

namespace Infomaniak\TrelloKanban\Models;

/**
 * Class Member
 *
 * @package Infomaniak\TrelloKanban\Models
 */
class MemberModel extends KanbanModel implements \JsonSerializable
{
    public $id;
    public $memberTrelloId;
    public $fullName;
    public $username;

    /**
     * @param $trello
     *
     * @return MemberModel
     */
    public static function retrieveFromTrello($trello): MemberModel
    {
        $model                 = new self();
        $model->id             = null;
        $model->memberTrelloId = $trello['id'];
        $model->fullName       = $trello['fullName'];
        $model->username       = $trello['username'];

        return $model;
    }
}
