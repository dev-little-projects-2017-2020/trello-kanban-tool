<?php

namespace Infomaniak\TrelloKanban\Models;

/**
 * Class LabelModel
 *
 * @package Infomaniak\TrelloKanban\KanbanModels
 */
class LabelModel
{
    public $id;
    public $labelTrelloId;
    public $boardId;
    public $name;
    public $color;
}
