<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 2020-02-11
 * Time: 05:46
 */

namespace Infomaniak\TrelloKanban\Models;

/**
 * Class CardMovement
 *
 * @package Infomaniak\TrelloKanban\Models
 */
class CardMovement
{
    public $id;
    public $date;
    public $cardId;
    public $cardName;
    public $listSource;
    public $listDest;
}
