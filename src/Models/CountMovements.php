<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 2020-02-09
 * Time: 07:52
 */

namespace Infomaniak\TrelloKanban\Models;

/**
 * Class CountMovements
 *
 * @package Infomaniak\TrelloKanban\Models
 */
class CountMovements
{
    public $count;
    public $sourceType;
    public $destType;
    public $forth;
}
