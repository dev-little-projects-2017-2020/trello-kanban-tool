<?php

namespace Infomaniak\TrelloKanban\Models;

/**
 * Class ListModel
 *
 * @package Infomaniak\TrelloKanban\KanbanModels
 */
class ListModel extends KanbanModel implements \JsonSerializable
{
    public $id;
    public $listTrelloId;
    public $name;
    public $boardId;
    public $type;

    /**
     * @param $trello
     *
     * @return ListModel
     */
    public static function retrieveFromTrello($trello): ListModel
    {
        $model               = new self();
        $model->id           = null;
        $model->listTrelloId = $trello['id'];
        $model->name         = $trello['name'];
        $model->boardId      = null;
        $model->type         = null;

        return $model;
    }
}
