<?php

namespace Infomaniak\TrelloKanban\Models;

/**
 * Class Environment
 *
 * @package Infomaniak\TrelloKanban\KanbanModels
 */
class BoardModel extends KanbanModel implements \JsonSerializable
{
    public $id;
    public $name;
    public $boardTrelloId;
    public $lastUpdatedAt;

    /**
     * @param $trello
     *
     * @return BoardModel
     */
    public static function retrieveFromTrello($trello): BoardModel
    {
        $model                = new self();
        $model->id            = null;
        $model->name          = $trello['name'];
        $model->boardTrelloId = $trello['id'];
        $model->lastUpdatedAt = null;

        return $model;
    }
}
