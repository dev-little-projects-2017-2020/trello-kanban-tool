<?php

namespace Infomaniak\TrelloKanban\Models;

/**
 * Class CountCardsModel
 *
 * @package Infomaniak\TrelloKanban\Models
 */
class CountCardsModel
{
    public $id;
    public $structId;
    public $date;
    public $listType;
    public $count;
    public $cardType;
}
