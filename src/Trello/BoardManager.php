<?php

namespace Infomaniak\TrelloKanban\Trello;

/**
 * Class BoardManager
 *
 * @package Infomaniak\TrelloKanban\Trello
 */
class BoardManager
{
    private $boardId;

    /**
     * BoardManager constructor.
     *
     * @param $boardId
     */
    public function __construct($boardId)
    {
        $this->boardId = $boardId;
    }

    /**
     * @return TrelloRequest
     */
    public function delete()
    {
        return new TrelloRequest(TrelloClient::DELETE, '/boards/' . $this->boardId);
    }

    /**
     * @param array $fields
     *
     * @return TrelloRequest
     */
    public function get($fields = []): TrelloRequest
    {
        $fields = empty($fields) ? 'all' : implode(',', $fields);
        return new TrelloRequest(TrelloClient::GET, '/board/' . $this->boardId, ['fields' => $fields]);
    }

    /**
     * @param array $fields
     * @param array $actions
     * @param null  $since
     * @param null  $before
     *
     * @return TrelloRequest
     */
    public function getActions($fields = [], $actions = [], $since = null, $before = null): TrelloRequest
    {
        $fields  = empty($fields) ? 'all' : implode(',', $fields);
        $actions = empty($actions) ? 'all' : implode(',', $actions);
        return new TrelloRequest(
            TrelloClient::GET,
            '/board/' . $this->boardId . '/actions',
            ['fields' => $fields, 'actions' => $actions, 'since' => $since, 'before' => $before]
        );
    }

    /**
     * @return TrelloRequest
     */
    public function getCards(): TrelloRequest
    {
        return new TrelloRequest(TrelloClient::GET, '/board/' . $this->boardId . '/cards');
    }

    /**
     * @param array $fields
     *
     * @return TrelloRequest
     */
    public function getLists($fields = []): TrelloRequest
    {
        $fields = empty($fields) ? 'all' : implode(',', $fields);
        return new TrelloRequest(TrelloClient::GET, '/board/' . $this->boardId . '/lists', ['fields' => $fields]);
    }

    /**
     * @param $name
     *
     * @return TrelloRequest
     */
    public function createList($name): TrelloRequest
    {
        return new TrelloRequest(TrelloClient::POST, '/boards/' . $this->boardId . '/lists', ['name' => $name, 'pos' => 'bottom']);
    }

    /**
     * @param $name
     * @param $color
     *
     * @return TrelloRequest
     */
    public function createLabel($name, $color)
    {
        return new TrelloRequest(TrelloClient::POST, '/boards/' . $this->boardId . '/labels', ['name' => $name, 'color' => $color]);
    }

    public function id()
    {
        return $this->boardId;
    }
}
