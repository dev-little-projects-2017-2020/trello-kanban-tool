<?php

namespace Infomaniak\TrelloKanban\Trello;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Infomaniak\TrelloKanban\Tools\Config;
use Infomaniak\TrelloKanban\Tools\Injector;
use Infomaniak\TrelloKanban\Tools\TrelloKanbanException;
use function GuzzleHttp\Psr7\build_query;

/**
 * Class TrelloClient
 *
 * @package Infomaniak\TrelloKanban\Tools
 */
class TrelloClient
{
    const GET    = 'GET';
    const POST   = 'POST';
    const PUT    = 'PUT';
    const DELETE = 'DELETE';
    private $key;
    private $token;
    private $baseUrl = 'https://api.trello.com/1';
    /** @var Client */
    private $http;
    /** @var TrelloRequest[] */
    private $differed = [];

    /**
     * TrelloClient constructor.
     *
     * @throws \InvalidArgumentException
     */
    public function __construct()
    {
        $config      = Config::trelloAuth();
        $this->key   = $config['key'];
        $this->token = $config['token'];
        $this->http  = new Client();
    }

    /**
     * @param TrelloRequest $request
     *
     * @return TrelloClient
     */
    public function differ(TrelloRequest $request)
    {
        if ($request->method() !== self::GET) {
            throw new TrelloKanbanException('can_only_differ_get_request');
        }
        $this->differed[] = $request;
        return $this;
    }

    /**
     * @return bool
     */
    public function batchGetClean()
    {
        /** @var TrelloRequest[][] $groups */
        $groups = array_chunk($this->differed, 10, false);
        foreach ($groups as $group) {
            $queryParams  = [
                'urls' => implode(',', array_map(function (TrelloRequest $request) {
                    $q = empty($request->queryParams()) ? '' : ('?' . build_query($request->queryParams()));
                    $q = $request->url() . $q;
                    $q = str_replace(',', '%2C', $q);
                    //$q = urlencode($q);
                    return $q;
                }, $group))
            ];
            $batchRequest = new TrelloRequest(self::GET, '/batch', $queryParams);
            $responses    = $this->request($batchRequest);
            foreach ($group as $key => $request) {
                $request->setResponse($responses[$key]['200']);
            }
        }
        return true;
    }

    /**
     * @param TrelloRequest $request
     *
     * @return array
     */
    public function request(TrelloRequest $request)
    {
        $this->waitUntilRateLimitIsOk();
        $url                  = $this->baseUrl . $request->url();
        $queryParams          = $request->queryParams();
        $queryParams['key']   = $this->key;
        $queryParams['token'] = $this->token;

        $params = ['query' => $queryParams];
        if (!empty($request->bodyParams())) {
            $params['json'] = $request->bodyParams();
        }

        try {
            $response = $this->http->request($request->method(), $url, [
                'query' => $queryParams
            ]);
        } catch (ClientException $exception) {
            $response = $exception->getResponse();
            throw new TrelloKanbanException('error', TrelloKanbanException::ERROR, json_decode($response->getBody(), true), $response->getStatusCode());
        }

        return json_decode($response->getBody()->getContents(), true);
    }

    private function waitUntilRateLimitIsOk()
    {
        $counter = Injector::counter(10, 90);
        if (!$counter->incr()) {
            $counter->wait();
        }
        if ($counter->reached()) {
            throw new TrelloKanbanException('rate_limit');
        }
    }
}
