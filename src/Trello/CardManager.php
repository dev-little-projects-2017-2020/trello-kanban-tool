<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 2020-02-08
 * Time: 06:41
 */

namespace Infomaniak\TrelloKanban\Trello;

use Carbon\Carbon;

/**
 * Class CardMarnager
 *
 * @package Infomaniak\TrelloKanban\Trello
 */
class CardManager
{
    private $cardId;

    /**
     * ListManager constructor.
     *
     * @param $cardId
     */
    public function __construct($cardId)
    {
        $this->cardId = $cardId;
    }

    /**
     * @return TrelloRequest
     */
    public function get()
    {
        return new TrelloRequest(
            TrelloClient::GET,
            '/cards/' . $this->cardId
        );
    }

    /**
     * @param string $labelId
     *
     * @return TrelloRequest
     */
    public function addLabel(string $labelId): TrelloRequest
    {
        return new TrelloRequest(
            TrelloClient::POST,
            '/cards/' . $this->cardId . '/idLabels',
            [
                'value' => $labelId
            ]
        );
    }

    /**
     * @param string $labelId
     *
     * @return TrelloRequest
     */
    public function removeLabel(string $labelId): TrelloRequest
    {
        return new TrelloRequest(
            TrelloClient::DELETE,
            '/cards/' . $this->cardId . '/idLabels',
            [
                'idLabel' => $labelId
            ]
        );
    }

    /**
     * @param Carbon $date
     *
     * @return TrelloRequest
     */
    public function updateDueDate(Carbon $date): TrelloRequest
    {
        return new TrelloRequest(
            TrelloClient::PUT,
            '/cards/' . $this->cardId,
            [
                'due' => $date->format('Y-m-d')
            ]
        );
    }

    /**
     * @param string $listId
     *
     * @return TrelloRequest
     */
    public function moveToList(string $listId): TrelloRequest
    {
        return new TrelloRequest(
            TrelloClient::PUT,
            '/cards/' . $this->cardId,
            [
                'idList' => $listId
            ]
        );
    }

    /**
     * @param string $boardId
     * @param string $listId
     *
     * @return TrelloRequest
     */
    public function moveToBoard(string $boardId, string $listId): TrelloRequest
    {
        return new TrelloRequest(
            TrelloClient::PUT,
            '/cards/' . $this->cardId,
            [
                'idBoard' => $boardId,
                'idList'  => $listId
            ]
        );
    }

    /**
     * @return TrelloRequest
     */
    public function archive(): TrelloRequest
    {
        return new TrelloRequest(
            TrelloClient::PUT,
            '/cards/' . $this->cardId,
            [
                'closed' => true
            ]
        );
    }

    /**
     * @param string $listId
     *
     * @return TrelloRequest
     */
    public function unarchive(string $listId): TrelloRequest
    {
        return new TrelloRequest(
            TrelloClient::PUT,
            '/cards/' . $this->cardId,
            [
                'closed' => false,
                'idList' => $listId
            ]
        );
    }
}
