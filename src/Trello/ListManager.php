<?php

namespace Infomaniak\TrelloKanban\Trello;

use Carbon\Carbon;

/**
 * Class ListManager
 *
 * @package Infomaniak\TrelloKanban\Trello
 */
class ListManager
{
    private $listId;

    /**
     * ListManager constructor.
     *
     * @param $listId
     */
    public function __construct($listId)
    {
        $this->listId = $listId;
    }

    /**
     * @param        $name
     * @param        $description
     * @param Carbon $due
     * @param        $labels
     * @param        $members
     *
     * @return TrelloRequest
     */
    public function createCard($name, $description, $due, $labels, $members): TrelloRequest
    {
        return new TrelloRequest(
            TrelloClient::POST,
            '/cards',
            [
                'idList'    => $this->id(),
                'name'      => $name,
                'desc'      => $description,
                'pos'       => 'bottom',
                'due'       => is_null($due) ? null : $due->format('Y-m-d'),
                'idLabels'  => $labels,
                'idMembers' => $members
            ]
        );
    }

    /**
     * @return string
     */
    public function id(): string
    {
        return $this->listId;
    }

    /**
     * @return TrelloRequest
     */
    public function getCards(): TrelloRequest
    {
        return new TrelloRequest(TrelloClient::GET, '/lists/' . $this->id() . '/cards', ['fields' => 'name,desc,labels']);
    }

    /**
     * @param $newName
     *
     * @return TrelloRequest
     */
    public function rename($newName): TrelloRequest
    {
        return new TrelloRequest(TrelloClient::PUT, '/list/' . $this->id() . '/name', ['value' => $newName]);
    }
}
