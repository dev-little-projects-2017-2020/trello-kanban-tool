<?php

namespace Infomaniak\TrelloKanban\Trello;

use Infomaniak\TrelloKanban\Tools\Injector;
use Infomaniak\TrelloKanban\Tools\TrelloKanbanException;

/**
 * Class TrelloRequest
 *
 * @package Infomaniak\TrelloKanban\Tools
 */
class TrelloRequest
{
    private $method      = '';
    private $url         = '';
    private $queryParams = [];
    private $bodyParams  = [];
    /** @var TrelloClient */
    private $client;
    /** @var array */
    private $response = [];

    /**
     * TrelloRequest constructor.
     *
     * @param       $method
     * @param       $url
     * @param array $queryParams
     * @param array $bodyParams
     */
    public function __construct($method, $url, $queryParams = [], $bodyParams = [])
    {
        $this->client      = Injector::trello();
        $this->method      = $method;
        $this->url         = $url;
        $this->queryParams = $queryParams;
        $this->bodyParams  = $bodyParams;
    }

    /**
     * @return TrelloRequest
     */
    public function batch(): TrelloRequest
    {
        $this->client->differ($this);
        return $this;
    }

    /**
     * @return array
     * @throws TrelloKanbanException
     */
    public function exec(): array
    {
        return $this->client->request($this);
    }

    /**
     * @return string
     */
    public function method(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function url(): string
    {
        return $this->url;
    }

    /**
     * @return array
     */
    public function queryParams(): array
    {
        return $this->queryParams;
    }

    /**
     * @return array
     */
    public function bodyParams(): array
    {
        return $this->bodyParams;
    }

    /**
     * @param $reponse
     */
    public function setResponse($reponse): void
    {
        $this->response = $reponse;
    }

    /**
     * @return array
     */
    public function response(): array
    {
        return $this->response;
    }
}
