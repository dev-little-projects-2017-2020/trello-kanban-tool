<?php

namespace Infomaniak\TrelloKanban\Trello;

use Infomaniak\TrelloKanban\Tools\Config;

/**
 * Class BoardManager
 *
 * @package Infomaniak\TrelloKanban\Trello
 */
class EnvironmentManager
{

    /**
     * @return TrelloRequest
     */
    public function getMembers(): TrelloRequest
    {
        return new TrelloRequest(TrelloClient::GET, '/organizations/' . Config::trelloOrganization() . '/members');
    }

    /**
     * @param $name
     *
     * @return TrelloRequest
     */
    public function createBoard($name): TrelloRequest
    {
        return new TrelloRequest(
            TrelloClient::POST,
            '/boards/',
            ['name' => $name, 'defaultLabels' => false, 'defaultLists' => false, 'idOrganization' => Config::trelloOrganization()]
        );
    }

    /**
     * @return TrelloRequest
     */
    public function getAllBoards(): TrelloRequest
    {
        return new TrelloRequest(TrelloClient::GET, '/organizations/' . Config::trelloOrganization() . '/boards');
    }
}
