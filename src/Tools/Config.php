<?php

namespace Infomaniak\TrelloKanban\Tools;

/**
 * Class Config
 *
 * @package Infomaniak\TrelloKanban\Tools
 */
class Config
{
    private static $database = [];
    private static $trello   = [];
    private static $counter  = [];

    /**
     * @return array
     */
    public static function database(): array
    {
        if (empty(self::$database)) {
            self::$database = [
                'host' => getenv('DB_HOST_TRELLO'),
                'name' => getenv('DB_NAME_TRELLO'),
                'user' => getenv('DB_USER_TRELLO'),
                'pass' => getenv('DB_PASS_TRELLO'),
            ];
        }
        return self::$database;
    }

    /**
     * @return array
     */
    public static function trelloAuth(): array
    {
        if (empty(self::$trello)) {
            self::$trello = [
                'key'   => getenv('TRELLO_KEY'),
                'token' => getenv('TRELLO_TOKEN'),
                'board' => getenv('TRELLO_BOARD')
            ];
        }
        return self::$trello;
    }

    /**
     * @return string
     */
    public static function trelloOrganization(): string
    {
        return getenv('TRELLO_ORGANIZATION');
    }

    /**
     * @return array
     */
    public static function counter(): array
    {
        if (empty(self::$counter)) {
            self::$counter = [
                'host' => getenv('REDIS_HOST'),
                'port' => getenv('REDIS_PORT'),
                'key'  => getenv('TRELLO_TOKEN')
            ];
        }
        return self::$counter;
    }
}
