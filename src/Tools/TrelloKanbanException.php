<?php

namespace Infomaniak\TrelloKanban\Tools;

/**
 * Class TrelloKanbanException
 *
 * @package Infomaniak\TrelloKanban\Tools
 */
class TrelloKanbanException extends \Exception
{
    const ERROR      = 0; //Une erreur qu'on doit signaler
    const MUTE_ERROR = 1; //Une erreur qu'on ne doit pas signaler car c'est normal de la rencontrer (eg : paypal 10486)
    const WARNING    = 2; //Pas vraiment une erreur, on log le problème et on continue
    const LOG        = 3; //Informations complémentaire qu'on souhaite garder en cas de besoin

    const UNDEFINED_DEPENDENCY = 'undefined_dependency';
    protected $context;
    protected $description;
    protected $level;

    /**
     * TrelloKanbanException constructor.
     *
     * @param string     $message
     * @param int        $level
     * @param string     $description
     * @param array|null $context
     * @param int        $code
     */
    public function __construct($message = 'trello_error', $level = self::ERROR, $description = '', $context = [], $code = 500)
    {
        $this->context     = $context;
        $this->description = $description;
        $this->level       = $level;
        parent::__construct($message, $code);
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }
}
