<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 2020-02-09
 * Time: 10:17
 */

namespace Infomaniak\TrelloKanban\Tools;

use Carbon\Carbon;

/**
 * Class TimeRange
 *
 * @package Infomaniak\TrelloKanban\Tools
 */
class TimeRange
{
    const PERIOD_SECOND  = 'second';
    const PERIOD_MINUTE  = 'minute';
    const PERIOD_HOUR    = 'hour';
    const PERIOD_DAY     = 'day';
    const PERIOD_WEEK    = 'week';
    const PERIOD_MONTH   = 'month';
    const PERIOD_QUARTER = 'quarter';
    const PERIOD_YEAR    = 'year';

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @param        $period
     *
     * @return Carbon[][]
     * @throws \InvalidArgumentException
     */
    public static function getRanges(Carbon $start, Carbon $end, $period)
    {
        $ranges = [];
        $start  = self::getRangeStart($start, $period);
        $next   = self::getNext($start, $period);
        while ($start->getTimestamp() < $end->getTimestamp()) {
            $ranges[] = [$start, $next];
            $start    = (clone $next);
            $next     = self::getNext($next, $period);
        }
        return $ranges;
    }

    /**
     * @param Carbon $desiredStart
     * @param        $period
     *
     * @return Carbon
     * @throws \InvalidArgumentException
     */
    private static function getRangeStart(Carbon $desiredStart, $period): Carbon
    {
        $start = clone $desiredStart;
        switch ($period) {
            case self::PERIOD_SECOND:
                return $start;
                break;
            case self::PERIOD_MINUTE:
                $start->setTime($start->hour, $start->minute, 0);
                return $start;
            case self::PERIOD_HOUR:
                $start->setTime($start->hour, 0, 0);
                return $start;
            case self::PERIOD_DAY:
                $start->setTime(0, 0, 0);
                return $start;
            case self::PERIOD_WEEK:
                $start->setTime(0, 0, 0);
                $start->subDays($start->dayOfWeekIso - 1);
                return $start;
            case self::PERIOD_MONTH:
                $start->setTime(0, 0, 0);
                $start->subDays($start->day - 1);
                return $start;
            case self::PERIOD_QUARTER: //1er janvier //1er avril //1er juillet //1er octobre
                $start->setTime(0, 0, 0);
                $first  = Carbon::create($start->year, 1, 1);
                $second = Carbon::create($start->year, 4, 1);
                $third  = Carbon::create($start->year, 7, 1);
                $fourth = Carbon::create($start->year, 10, 1);
                if ($start->getTimestamp() < $second->getTimestamp()) {
                    return $first;
                }
                if ($start->getTimestamp() < $third->getTimestamp()) {
                    return $second;
                }
                if ($start->getTimestamp() < $fourth->getTimestamp()) {
                    return $third;
                }
                return $fourth;
            case self::PERIOD_YEAR:
                $start->setTime(0, 0, 0);
                $start->subDays($start->dayOfYear - 1);
                return $start;
        }
        return $start;
    }

    /**
     * @param Carbon $current
     * @param        $period
     *
     * @return Carbon
     */
    private static function getNext(Carbon $current, $period): Carbon
    {
        $next = clone $current;
        switch ($period) {
            case self::PERIOD_SECOND:
                return $next->addSecond();
                break;
            case self::PERIOD_MINUTE:
                return $next->addMinute();
                break;
            case self::PERIOD_HOUR:
                return $next->addHour();
                break;
            case self::PERIOD_DAY:
                return $next->addDay();
                break;
            case self::PERIOD_WEEK:
                return $next->addWeek();
                break;
            case self::PERIOD_MONTH:
                return $next->addMonth();
                break;
            case self::PERIOD_QUARTER:
                return $next->addQuarter();
                break;
            case self::PERIOD_YEAR:
                return $next->addYear();
                break;
        }
        return $next->addMonth();
    }
}
