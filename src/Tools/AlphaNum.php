<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 2020-01-26
 * Time: 09:43
 */

namespace Infomaniak\TrelloKanban\Tools;

/**
 * Class AlphaNum
 *
 * @package Infomaniak\TrelloKanban\Tools
 */
class AlphaNum
{

    /**
     * @param $string
     *
     * @return string|string[]|null
     */
    public static function sanitize($string)
    {
        return preg_replace('/[^a-zA-Z0-9]+/', '', $string);
    }
}
