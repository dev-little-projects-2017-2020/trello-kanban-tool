<?php

namespace Infomaniak\TrelloKanban\Tools;

use Predis\Client;

/**
 * Class Counter
 *
 * @package Infomaniak\TrelloKanban\Tools
 */
class Counter
{

    /** @var Client */
    private $redis;

    private $key;
    private $ttl = 10;
    private $max = 90;

    /**
     * Counter constructor.
     *
     * @param      $ttl
     * @param      $max
     * @param null $key
     */
    public function __construct($ttl, $max, $key = null)
    {
        $config      = Config::counter();
        $server      = "tcp://" . $config['host'] . ':' . $config['port'];
        $this->key   = 'counter:' . (is_null($key) ? Config::trelloAuth()['token'] : $key);
        $this->ttl   = $ttl;
        $this->max   = $max;
        $this->redis = new Client($server);
    }

    /**
     * @return bool
     */
    public function incr(): bool
    {
        $counterKey = $this->key . ':' . microtime(true);
        $this->redis->set($counterKey, 0);
        $this->redis->expire($counterKey, $this->ttl);
        return !($this->reached());
    }

    /**
     * @return bool
     */
    public function reached()
    {
        return $this->value() > $this->max;
    }

    /**
     * @return int
     */
    public function value(): int
    {
        return count($this->redis->keys($this->key . ':*'));
    }

    /**
     *
     */
    public function wait(): void
    {
        $entries = $this->redis->keys($this->key . ':*');
        usort($entries, function ($entryA, $entryB) {
            $entryA = intval(end(explode(':', $entryA)));
            $entryB = intval(end(explode(':', $entryB)));
            return $entryA - $entryB;
        });
        $median = intval($this->max / 2);
        if (isset($entries[$median])) {
            $ttl = $this->redis->pttl($entries[$median]) * 1000;
            $ttl = $ttl < 100 ? 100 : $ttl;
            usleep($ttl);
        }
        return;
    }

    public function clean()
    {
        $keys = $this->redis->keys($this->key . '*');
        if (!empty($keys)) {
            $this->redis->del($keys);
        }
    }
}
