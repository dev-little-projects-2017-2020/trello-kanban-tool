<?php

namespace Infomaniak\TrelloKanban\Tools;

use PDO;

/**
 * Class DB
 *
 * @package Infomaniak\TrelloKanban\Tools
 */
class DB extends PDO
{
    const OBJECT       = PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE;
    const BOUND        = PDO::FETCH_BOUND;
    const BOUND_OBJECT = PDO::FETCH_INTO;
    const COLUMN       = PDO::FETCH_COLUMN;

    /**
     * DB constructor.
     *
     */
    public function __construct()
    {
        $config = Config::database();
        $host   = $config['host'];
        $name   = $config['name'];
        $user   = $config['user'];
        $pass   = $config['pass'];
        parent::__construct('mysql:host=' . $host . ';dbname=' . $name . ';charset=utf8mb4', $user, $pass);
    }

    /**
     * @param string $query
     * @param array  $params
     *
     * @return mixed|null
     */
    public function selectValue(string $query, array $params = [])
    {
        $statement = $this->prepare($query);
        if ($statement->execute($params)) {
            return $statement->fetch(self::COLUMN);
        }
        $error = $statement->errorInfo();
        if ($error) {
            throw new TrelloKanbanException($error[2]);
        }
        return null;
    }

    /**
     * @param string $query
     * @param array  $params
     * @param null   $foundRows
     * @param int    $column
     *
     * @return array
     */
    public function selectColumn(string $query, array $params = [], &$foundRows = null, $column = 0): array
    {
        if (!is_null($foundRows) && !strpos($query, 'SQL_CALC_FOUND_ROWS')) {
            $query = 'SELECT SQL_CALC_FOUND_ROWS ' . substr($query, 7);
        }
        $statement = $this->prepare($query);
        if ($statement->execute($params)) {
            $foundRows = is_null($foundRows) ? null : $this->query('SELECT FOUND_ROWS()');
            return $statement->fetchAll(self::COLUMN, $column);
        }
        return [];
    }

    /**
     * @param string $query
     * @param string $className
     * @param array  $params
     * @param null   $foundRows
     *
     * @return \stdClass[]|$className[]
     * @throws TrelloKanbanException
     */
    public function selectAll(string $query, string $className = null, array $params = [], &$foundRows = null): array
    {
        if (!is_null($foundRows) && !strpos($query, 'SQL_CALC_FOUND_ROWS')) {
            $query = 'SELECT SQL_CALC_FOUND_ROWS ' . substr($query, 7);
        }
        $statement = $this->prepare($query);
        if ($statement->execute($params)) {
            $foundRows = is_null($foundRows) ? null : $this->query('SELECT FOUND_ROWS()');
            return $statement->fetchAll(self::OBJECT, $className);
        }
        $error = $statement->errorInfo();
        if ($error) {
            throw new TrelloKanbanException($error[2]);
        }
        return null;
    }

    /**
     * @param string      $query
     * @param array|null  $params
     * @param string|null $className
     *
     * @return mixed|null
     */
    public function selectOne(string $query, string $className = null, array $params = null)
    {
        $statement = $this->prepare($query);
        if ($statement->execute($params)) {
            $statement->setFetchMode(self::OBJECT, $className);
            return $statement->fetch();
        }
        $error = $statement->errorInfo();
        if ($error) {
            throw new TrelloKanbanException($error[2]);
        }
        return null;
    }

    /**
     * @param string $query
     * @param array  $params
     *
     * @return int
     */
    public function update(string $query, array $params = []): int
    {
        $statement = $this->prepare($query);
        if ($statement->execute($params)) {
            return $statement->rowCount();
        }
        $error = $statement->errorInfo();
        if ($error) {
            throw new TrelloKanbanException($error[2]);
        }
        return 0;
    }

    /**
     * @param string $query
     * @param array  $params
     *
     * @return int|string
     */
    public function insert(string $query, array $params = [])
    {
        $statement = $this->prepare($query);
        if ($statement->execute($params)) {
            return $this->lastInsertId();
        }
        $error = $statement->errorInfo();
        if ($error) {
            throw new TrelloKanbanException($error[2]);
        }
        return 0;
    }

    /**
     * @return string
     */
    public function getDatabaseName(): string
    {
        return $this->query('select database()')->fetchColumn();
    }
}
