<?php

namespace Infomaniak\TrelloKanban\Tools;

use Carbon\Carbon;

/**
 * Class Date
 *
 * @package Infomaniak\TrelloKanban\Tools
 */
class Date
{

    /**
     * @param string $date
     *
     * @return Carbon
     * @throws \Exception
     */
    public static function createFromTrello(string $date): Carbon
    {
        $date = explode('T', $date);
        $time = substr($date[1], 0, 8);
        $date = $date[0];
        return self::createFromDB($date . ' ' . $time);
    }

    /**
     * @param string $date
     *
     * @return Carbon
     */
    public static function createFromDB(string $date): Carbon
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date);
    }

    /**
     * @param Carbon $date
     *
     * @return string
     */
    public static function toDB(Carbon $date): string
    {
        return $date->format('Y-m-d H:i:s');
    }
}
