<?php

namespace Infomaniak\TrelloKanban\Tools;

use Infomaniak\TrelloKanban\Trello\TrelloClient;

/**
 * Class Injector
 *
 * Pattern Injection de dépendances
 *
 * @package Infomaniak\TrelloKanban\Tools
 */
class Injector
{

    /** @var DB */
    private static $database = null;
    /** @var TrelloClient */
    private static $trello = null;
    /** @var Counter[] */
    private static $counter = [];

    /**
     * @return DB
     */
    public static function database(): DB
    {
        if (!isset(self::$database)) {
            self::$database = new DB();
        }
        return self::$database;
    }

    /**
     * @return TrelloClient
     */
    public static function trello(): TrelloClient
    {
        if (!isset(self::$trello)) {
            self::$trello = new TrelloClient();
        }
        return self::$trello;
    }

    /**
     * @param      $ttl
     * @param      $max
     *
     * @param null $key
     *
     * @return Counter
     */
    public static function counter($ttl, $max, $key = null): Counter
    {
        if (!isset(self::$counter[$ttl . '-' . $max])) {
            self::$counter[$ttl . '-' . $max] = new Counter($ttl, $max, $key);
        }
        return self::$counter[$ttl . '-' . $max];
    }
}
