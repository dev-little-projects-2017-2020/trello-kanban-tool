<?php

namespace Infomaniak\TrelloKanban\Features;

use Infomaniak\TrelloKanban\Models\BoardModel;
use Infomaniak\TrelloKanban\Models\ListModel;
use Infomaniak\TrelloKanban\Models\MemberModel;
use Infomaniak\TrelloKanban\Tools\Injector;
use Infomaniak\TrelloKanban\Trello\BoardManager;
use Infomaniak\TrelloKanban\Trello\EnvironmentManager;

/**
 * Class KanbanBoard
 *
 * @package Infomaniak\TrelloKanban\Features
 */
class KanbanBoard
{

    /**
     * @param $name
     *
     * @return BoardModel
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     */
    public static function createBoard($name): BoardModel
    {
        $manager = new EnvironmentManager();
        $board   = $manager->createBoard($name)->exec();

        $query = 'INSERT INTO Boards (name, boardTrelloId) VALUES (:envName, :boardId)';
        Injector::database()->insert($query, ['envName' => $name, 'boardId' => $board['id']]);
        $boardId = Injector::database()->lastInsertId();

        $boardManager = new BoardManager($board['id']);
        KanbanList::createAll($boardManager, $boardId);
        KanbanLabel::createAll($boardManager, $boardId);

        $actions = $boardManager->getActions()->exec();
        $last    = end($actions);
        $query   = 'UPDATE Boards SET lastUpdatedAt = :lastActionId WHERE id = :boardId';
        Injector::database()->update($query, ['lastActionId' => $last['id'], 'boardId' => $boardId]);

        return self::getBoard($boardId);
    }

    /**
     * @param $id
     *
     * @return BoardModel
     */
    public static function getBoard($id): BoardModel
    {
        $query   = 'SELECT * FROM Boards WHERE id = :id';
        $results = Injector::database()->selectOne($query, BoardModel::class, ['id' => $id]);
        return $results;
    }

    /**
     * @param $boardId
     * @param $listToType
     * @param $activeMembers
     * @param $colors
     *
     * @return BoardModel
     */
    public static function createFromTrelloBoard(
        $boardId,
        $listToType = [],
        $activeMembers = [],
        $colors = [KanbanLabel::GREEN, KanbanLabel::BLUE, KanbanLabel::ORANGE, KanbanLabel::PURPLE]
    ): BoardModel {
        $boardManager = new BoardManager($boardId);
        $board        = $boardManager->get()->exec();

        //Insert Board
        $query = 'INSERT INTO Boards (name, boardTrelloId, lastUpdatedAt) VALUES (:envName, :boardTrelloId, NULL)';
        Injector::database()->insert($query, ['envName' => $board['name'], 'boardTrelloId' => $board['id']]);
        $boardId = Injector::database()->lastInsertId();

        //Insert Members
        self::createMembersFromTrello($activeMembers);
        KanbanLabel::createAll($boardManager, $boardId, $colors);

        //Insert Lists
        $trelloListsResponse = $boardManager->getLists()->exec();
        $trelloLists         = [];
        foreach ($trelloListsResponse as $list) {
            $trelloLists[$list['id']] = $list;
        }
        $listToType = array_map(function ($listId) use ($trelloLists) {
            return $trelloLists[$listId];
        }, $listToType);
        KanbanList::initializeAll($boardId, $listToType);

        Actions::handleLastActions($boardId);

        return self::getBoard($boardId);
    }

    /**
     * @param array $availables
     *
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     */
    public static function createMembersFromTrello($availables = []): void
    {
        $manager         = new EnvironmentManager();
        $membersInTrello = $manager->getMembers()->exec();

        foreach ($membersInTrello as $member) {
            if (empty($availables) || in_array($member['id'], $availables)) {
                $query = 'INSERT INTO Members (memberTrelloId, fullName, username, available) VALUES (:memberTrelloId, :fullName, :username, :available)';
                Injector::database()->insert(
                    $query,
                    [
                        'memberTrelloId' => $member['id'],
                        'fullName'       => $member['fullName'],
                        'username'       => $member['username'],
                        'available'      => 1
                    ]
                );
            }
        }
    }

    /**
     * @return BoardModel[]
     */
    public static function getBoards(): array
    {
        $query   = 'SELECT * FROM Boards';
        $results = Injector::database()->selectAll($query, BoardModel::class);
        return $results;
    }

    /**
     * @param $boardTrelloId
     *
     * @return BoardModel
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     */
    public static function getFromTrello($boardTrelloId): BoardModel
    {
        $query   = 'SELECT * FROM Boards WHERE boardTrelloId = :boardTrelloId';
        $results = Injector::database()->selectOne($query, BoardModel::class, ['boardTrelloId' => $boardTrelloId]);
        return $results;
    }

    /**
     * @return array
     */
    public static function getBoardsFromTrello(): array
    {
        $manager = new EnvironmentManager();
        $boards  = $manager->getAllBoards()->batch();
        $members = $manager->getMembers()->batch();
        Injector::trello()->batchGetClean();
        $boards  = $boards->response();
        $members = $members->response();

        $boards = array_map(function ($trello) {
            return BoardModel::retrieveFromTrello($trello);
        }, $boards);

        $boards  = array_map(function (BoardModel $model) {
            $model->id = Injector::database()
                                 ->selectValue('SELECT id FROM Boards WHERE boardTrelloId = :boardTrelloId', ['boardTrelloId' => $model->boardTrelloId]);
            return $model;
        }, $boards);
        $members = array_map(function ($trello) {
            return MemberModel::retrieveFromTrello($trello);
        }, $members);

        return ['boards' => $boards, 'members' => $members, 'types' => KanbanList::$types];
    }

    /**
     * @param $boardTrelloId
     *
     * @return array
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     */
    public static function getBoardListsFromTrello($boardTrelloId): array
    {
        $boardManager = new BoardManager($boardTrelloId);
        $lists        = $boardManager->getLists()->exec();

        $lists = array_map(function ($trello) {
            return ListModel::retrieveFromTrello($trello);
        }, $lists);
        return ['lists' => $lists];
    }

    /**
     * @return MemberModel[]
     */
    public static function getMembers(): array
    {
        $query   = 'SELECT id, memberTrelloId, fullName, username FROM Members WHERE available = 1';
        $results = Injector::database()->selectAll($query, MemberModel::class);
        return $results;
    }

    /**
     * @return int
     */
    public static function countMembers(): int
    {
        $query   = 'SELECT COUNT(*) FROM Members WHERE available = 1';
        $members = Injector::database()->selectValue($query);
        return $members;
    }

    /**
     * @param $id
     *
     * @return MemberModel
     */
    public static function getMember($id): MemberModel
    {
        $query   = 'SELECT id, memberTrelloId, fullName, username FROM Members WHERE id = :id';
        $results = Injector::database()->selectOne($query, MemberModel::class, ['id' => $id]);
        return $results;
    }
}
