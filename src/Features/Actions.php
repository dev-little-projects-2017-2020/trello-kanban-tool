<?php /** @noinspection PhpUnusedParameterInspection */

namespace Infomaniak\TrelloKanban\Features;

use Infomaniak\TrelloKanban\Models\BoardModel;
use Infomaniak\TrelloKanban\Models\CardModel;
use Infomaniak\TrelloKanban\Models\ListModel;
use Infomaniak\TrelloKanban\Tools\Date;
use Infomaniak\TrelloKanban\Tools\Injector;
use Infomaniak\TrelloKanban\Tools\TrelloKanbanException;
use Infomaniak\TrelloKanban\Trello\BoardManager;
use Infomaniak\TrelloKanban\Trello\CardManager;

/**
 * Class Actions
 *
 * @package Infomaniak\TrelloKanban\Features
 */
class Actions
{

    /**
     * @param $boardId
     *
     * @throws TrelloKanbanException
     */
    public static function handleLastActions($boardId)
    {
        $board      = KanbanBoard::getBoard($boardId);
        $getActions = (new BoardManager($board->boardTrelloId))->getActions([], [], $board->lastUpdatedAt)->exec();
        $actions    = $getActions;
        while (count($getActions) === 1000) {
            $firstAction = reset($getActions);
            $getActions  = (new BoardManager($board->boardTrelloId))->getActions([], [], $board->lastUpdatedAt, $firstAction['id'])->exec();
            $actions     = array_merge($actions, $getActions);
        }
        $actions    = array_reverse($actions);
        $lastAction = $board->lastUpdatedAt;
        foreach ($actions as $action) {
            $date       = Date::createFromTrello($action['date']);
            $lastAction = $action['id'];
            switch ($action['type']) {
                case 'convertToCardFromCheckItem':
                case 'copyCard':
                case 'createCard':
                    self::handleNewCard($action['data'], $date);
                    break;
                case 'moveCardToBoard':
                    self::moveCardToBoard($action['data'], $date);
                    break;
                case 'updateCard':
                    self::updateCard($action['data'], $date);
                    break;
                case 'updateList':
                    self::updateList($action['data']);
                    break;
                case 'moveCardFromBoard':
                    self::moveCardFromBoard($action['data'], $date);
                    break;
            }
        }
        $query = 'UPDATE Boards SET lastUpdatedAt = :lastAction WHERE id = :boardId';
        Injector::database()->update($query, ['lastAction' => $lastAction, 'boardId' => $board->id]);

        self::updateLabels($boardId, $board);
    }

    /**
     * @param $data
     * @param $date
     *
     * @throws TrelloKanbanException
     */
    protected static function handleNewCard($data, $date)
    {
        $listModel = KanbanList::getFromTrello($data['list']['id']);
        if ($listModel) {
            $cardModel = CardModel::retrieveFromTrello((new CardManager($data['card']['id']))->get()->exec(), $listModel->id);
            KanbanCard::createFromTrello($cardModel, $date);
        }
    }

    /**
     * @param $data
     * @param $date
     *
     * @throws TrelloKanbanException
     */
    protected static function moveCardToBoard($data, $date)
    {
        $cardModel = KanbanCard::getFromTrello($data['card']['id']);
        if ($cardModel) {
            KanbanCard::registerMovement(KanbanCard::getFromTrello($data['card']['id']), KanbanList::getFromTrello($data['card']['idList']), $date);
            return;
        }
        self::handleNewCard($data, $date);
    }

    /**
     * Modify a card
     *
     * @param $data
     * @param $date
     *
     * @throws TrelloKanbanException
     */
    public static function updateCard($data, $date)
    {
        foreach (array_keys($data['old']) as $oldKey) {
            if ($oldKey == 'due') {
                self::updateDueDateOnCard($data);
                break;
            }
            if ($oldKey == 'idList') {
                $newListModel = KanbanList::getFromTrello($data['card']['idList']);
                self::updateListOnCard($data, $date, $newListModel);
                break;
            }
            if ($oldKey == 'closed') {
                $board        = KanbanBoard::getFromTrello($data['board']['id']);
                $newListModel = $data['card']['closed'] ? KanbanList::getByType($board->id, KanbanList::DONE) : KanbanList::getFromTrello($data['list']['id']);
                self::updateListOnCard($data, $date, $newListModel);
            }
        }
    }

    /**
     * @param $data
     *
     * @throws TrelloKanbanException
     */
    private static function updateDueDateOnCard($data): void
    {
        $query = 'UPDATE Cards SET dueAt = :dueAt WHERE cardTrelloId = :cardTrelloId';
        $dueAt = is_null($data['card']['due']) ? null : substr($data['card']['due'], 0, 10);
        Injector::database()->update($query, ['dueAt' => $dueAt, 'cardTrelloId' => $data['card']['id']]);
    }

    /**
     * @param           $data
     * @param           $date
     *
     * @param ListModel $newListModel
     *
     * @throws TrelloKanbanException
     */
    private static function updateListOnCard($data, $date, $newListModel): void
    {
        $cardModel = KanbanCard::getFromTrello($data['card']['id']);
        if ($cardModel && !$newListModel) {
            $board        = KanbanBoard::getFromTrello($data['board']['id']);
            $newListModel = KanbanList::getByType($board->id, KanbanList::DONE);
        }
        if (!$newListModel) {
            return;
        }
        if (!$cardModel) {
            self::handleNewCard($data, $date);
            return;
        }
        if ($cardModel->listId != $newListModel->id) {
            KanbanCard::registerMovement($cardModel, $newListModel, $date);
        }
    }

    /**
     * Modify a list
     *
     * @param $data
     *
     * @throws TrelloKanbanException
     */
    public static function updateList($data)
    {
        foreach (array_keys($data['old']) as $oldKey) {
            if ($oldKey == 'name') {
                $query = 'UPDATE Lists SET name = :newName WHERE listTrelloId = :listTrelloId';
                Injector::database()->update($query, ['newName' => $data['list']['name'], 'listTrelloId' => $data['list']['id']]);
            }
        }
    }

    /**
     * Move a card to another board
     *
     * @param $data
     * @param $date
     */
    public static function moveCardFromBoard($data, $date)
    {
        $cardModel = KanbanCard::getFromTrello($data['card']['id']);
        if ($cardModel) {
            $board = KanbanBoard::getFromTrello($data['board']['id']);
            KanbanCard::registerMovement($cardModel, KanbanList::getByType($board->id, KanbanList::DONE), $date);
        }
    }

    /**
     * @param                                            $boardId
     * @param BoardModel                                 $board
     *
     * @throws TrelloKanbanException
     */
    private static function updateLabels($boardId, BoardModel $board): void
    {
        $trelloCards = (new BoardManager($board->boardTrelloId))->getCards()->exec();
        /** @var CardModel[] $models */
        $models = Injector::database()
                          ->selectAll('SELECT c.* fROM Cards c LEFT JOIN Lists l on c.listId = l.id WHERE l.boardId = :boardId', CardModel::class, ['boardId' => $boardId]);
        /** @var CardModel[] $cardModels */
        $cardModels = [];
        foreach ($models as $cardModel) {
            $cardModels[$cardModel->cardTrelloId] = $cardModel;
        }

        foreach ($trelloCards as $trelloCard) {
            if ($cardModels[$trelloCard['id']]) {
                $cardModel   = $cardModels[$trelloCard['id']];
                $typeInitial = $cardModel->type;
                foreach ($trelloCard['labels'] as $label) {
                    $cardModel->type = isset(KanbanCard::$typeByLabel[$label['name']]) ? KanbanCard::$typeByLabel[$label['name']] : $cardModel->type;
                }
                if ($typeInitial != $cardModel->type) {
                    $query = 'UPDATE Cards c SET type = :type WHERE id = :cardId';
                    Injector::database()->update($query, ['type' => $cardModel->type, 'cardId' => $cardModel->id]);
                }
            }
        }
    }
}
