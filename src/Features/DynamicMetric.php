<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 2020-02-09
 * Time: 06:38
 */

namespace Infomaniak\TrelloKanban\Features;

use Carbon\Carbon;
use Infomaniak\TrelloKanban\Models\CardMovement;
use Infomaniak\TrelloKanban\Models\CountMovements;
use Infomaniak\TrelloKanban\Tools\Date;
use Infomaniak\TrelloKanban\Tools\Injector;
use Infomaniak\TrelloKanban\Tools\TimeRange;

/**
 * Class DynamicMetric
 *
 * @package Infomaniak\TrelloKanban\Features
 */
class DynamicMetric
{
    const GO_FORTH     = 'go_forth';
    const GO_BACK      = 'go_back';
    const GO_OUT       = 'go_out';
    const GO_FORBIDDEN = 'go_forbidden';
    public static $movementNature = [
        KanbanList::IDEAS      => [
            KanbanList::IDEAS      => self::GO_FORBIDDEN,
            KanbanList::PROJECTS   => self::GO_FORTH,
            KanbanList::TODO       => self::GO_FORTH,
            KanbanList::ON_GOING   => self::GO_FORTH,
            KanbanList::VALIDATING => self::GO_FORTH,
            KanbanList::DONE       => self::GO_OUT,
            KanbanList::DEBT       => self::GO_BACK,
        ],
        KanbanList::PROJECTS   => [
            KanbanList::IDEAS      => self::GO_BACK,
            KanbanList::PROJECTS   => self::GO_FORBIDDEN,
            KanbanList::TODO       => self::GO_FORTH,
            KanbanList::ON_GOING   => self::GO_FORTH,
            KanbanList::VALIDATING => self::GO_FORTH,
            KanbanList::DONE       => self::GO_OUT,
            KanbanList::DEBT       => self::GO_BACK,
        ],
        KanbanList::TODO       => [
            KanbanList::IDEAS      => self::GO_BACK,
            KanbanList::PROJECTS   => self::GO_FORTH,
            KanbanList::TODO       => self::GO_FORBIDDEN,
            KanbanList::ON_GOING   => self::GO_FORTH,
            KanbanList::VALIDATING => self::GO_FORTH,
            KanbanList::DONE       => self::GO_OUT,
            KanbanList::DEBT       => self::GO_BACK,
        ],
        KanbanList::ON_GOING   => [
            KanbanList::IDEAS      => self::GO_BACK,
            KanbanList::PROJECTS   => self::GO_BACK,
            KanbanList::TODO       => self::GO_BACK,
            KanbanList::ON_GOING   => self::GO_FORBIDDEN,
            KanbanList::VALIDATING => self::GO_FORTH,
            KanbanList::DONE       => self::GO_OUT,
            KanbanList::DEBT       => self::GO_BACK,
        ],
        KanbanList::VALIDATING => [
            KanbanList::IDEAS      => self::GO_BACK,
            KanbanList::PROJECTS   => self::GO_BACK,
            KanbanList::TODO       => self::GO_BACK,
            KanbanList::ON_GOING   => self::GO_BACK,
            KanbanList::VALIDATING => self::GO_FORBIDDEN,
            KanbanList::DONE       => self::GO_OUT,
            KanbanList::DEBT       => self::GO_BACK,
        ],
        KanbanList::DONE       => [
            KanbanList::IDEAS      => self::GO_BACK,
            KanbanList::PROJECTS   => self::GO_BACK,
            KanbanList::TODO       => self::GO_BACK,
            KanbanList::ON_GOING   => self::GO_BACK,
            KanbanList::VALIDATING => self::GO_BACK,
            KanbanList::DONE       => self::GO_OUT,
            KanbanList::DEBT       => self::GO_BACK,
        ],
        KanbanList::DEBT       => [
            KanbanList::IDEAS      => self::GO_FORTH,
            KanbanList::PROJECTS   => self::GO_FORTH,
            KanbanList::TODO       => self::GO_FORTH,
            KanbanList::ON_GOING   => self::GO_FORTH,
            KanbanList::VALIDATING => self::GO_FORTH,
            KanbanList::DONE       => self::GO_OUT,
            KanbanList::DEBT       => self::GO_FORBIDDEN,
        ],
    ];

    /**
     * @param int $boardId
     *
     * @return array
     */
    public static function listMovements(int $boardId): array
    {
        $query = 'SELECT cm.id, cm.date, c.id AS cardId, c.name AS cardName, lsource.name AS listSource, ldest.name AS listDest FROM CardsMovements cm 
                  JOIN Cards c on cm.cardId = c.id
                  LEFT JOIN Lists lsource ON cm.listSourceId = lsource.id
                  JOIN Lists ldest on cm.listDestId = ldest.id 
                  WHERE ldest.boardId = :boardId';
        return Injector::database()->selectAll($query, CardMovement::class, ['boardId' => $boardId]);
    }

    /**
     * @param int         $boardId
     * @param string      $period
     * @param Carbon|null $start
     * @param Carbon|null $end
     * @param string      $cardType
     *
     * @return array
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     * @throws \InvalidArgumentException
     */
    public static function cycleTime(int $boardId, string $period, Carbon $start = null, Carbon $end = null, string $cardType = null): array
    {
        list($start, $end) = self::getRangeLimits($boardId, $start, $end);
        $ranges = TimeRange::getRanges($start, $end, $period);
        $data   = [];
        /** @var Carbon[] $range */
        foreach ($ranges as $range) {
            $data[] = [
                'date'      => $range[0]->getTimestamp(),
                'cycleTime' => self::cycleTimeByRange($range[0], $range[1], $boardId, $cardType)
            ];
        }
        return $data;
    }

    /**
     * @param             $boardId
     * @param Carbon|null $start
     * @param Carbon|null $end
     *
     * @return Carbon[]
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     */
    private static function getRangeLimits($boardId, Carbon $start = null, Carbon $end = null): array
    {
        if (is_null($start)) {
            $selectFirstTime = 'SELECT cm.date FROM CardsMovements cm JOIN Lists l on cm.listDestId = l.id WHERE l.boardId = :boardId ORDER BY date ASC LIMIT 1';
            $start           = Injector::database()->selectValue($selectFirstTime, ['boardId' => $boardId]);
            $start           = Date::createFromDB($start);
        }
        if (is_null($end)) {
            $end = Carbon::now();
        }
        return [$start, $end];
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @param int    $boardId
     *
     * @param string $cardType
     *
     * @return int
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     */
    private static function cycleTimeByRange(Carbon $start, Carbon $end, int $boardId, string $cardType = null): int
    {
        $query  = 'SELECT SUM(UNIX_TIMESTAMP(cm2.date) - UNIX_TIMESTAMP(cm1.date)) / COUNT(*) AS cycleTime FROM CardsMovements cm1 
                  JOIN Cards c on cm1.cardId = c.id
                  JOIN CardsMovements cm2 ON cm2.cardId = cm1.cardId 
                  JOIN Lists l2 on cm2.listDestId = l2.id
                  JOIN Boards b on l2.boardId = b.id
                  WHERE cm1.listSourceId IS NULL AND l2.type = :typeDone AND b.id = :boardId AND cm2.date >= :start AND cm2.date <= :end AND c.type = :cardType';
        $query  = is_null($cardType) ? $query : ($query . ' AND c.type = :cardType');
        $params = [
            'boardId'  => $boardId,
            'start'    => $start,
            'end'      => $end,
            'typeDone' => KanbanList::DONE,
        ];
        is_null($cardType) ? null : $params['cardType'] = $cardType;
        $cycleTime = Injector::database()->selectValue($query, $params);
        return intval($cycleTime);
    }

    /**
     * @param int         $boardId
     * @param string      $period
     * @param Carbon|null $start
     * @param Carbon|null $end
     * @param string|null $cardType
     *
     * @return array
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     * @throws \InvalidArgumentException
     */
    public static function leadTime(int $boardId, string $period, Carbon $start = null, Carbon $end = null, string $cardType = null): array
    {
        list($start, $end) = self::getRangeLimits($boardId, $start, $end);
        $ranges = TimeRange::getRanges($start, $end, $period);
        $data   = [];
        /** @var Carbon[] $range */
        foreach ($ranges as $range) {
            $data[] = [
                'date'     => $range[0]->getTimestamp(),
                'leadTime' => self::leadTimeByRange($range[0], $range[1], $boardId, $cardType)
            ];
        }
        return $data;
    }

    /**
     * @param Carbon $start
     * @param Carbon $end
     * @param int    $boardId
     *
     * @param string $cardType
     *
     * @return int
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     */
    private static function leadTimeByRange(Carbon $start, Carbon $end, int $boardId, string $cardType = null): int
    {
        $query  = 'SELECT SUM(UNIX_TIMESTAMP(cm2.date) - UNIX_TIMESTAMP(cm1.date)) / COUNT(*) AS leadTime FROM CardsMovements cm1
                  JOIN Cards c on cm1.cardId = c.id
                  JOIN CardsMovements cm2 ON cm2.cardId = cm1.cardId
                  JOIN Lists l1 on cm1.listDestId = l1.id
                  JOIN Lists l2 on cm2.listDestId = l2.id
                  JOIN Boards b on l2.boardId = b.id
                  WHERE l1.type = :typeOnGoing AND l2.type = :typeDone AND b.id = :boardId AND cm2.date >= :start AND cm2.date <= :end';
        $query  = is_null($cardType) ? $query : ($query . ' AND c.type = :cardType');
        $params = [
            'boardId'     => $boardId,
            'start'       => $start,
            'end'         => $end,
            'typeDone'    => KanbanList::DONE,
            'typeOnGoing' => KanbanList::ON_GOING,
        ];
        is_null($cardType) ? null : $params['cardType'] = $cardType;

        $leadTime = Injector::database()->selectValue(
            $query,
            $params
        );
        return intval($leadTime);
    }

    /**
     * @param int         $boardId
     * @param string      $period
     * @param Carbon|null $start
     * @param Carbon|null $end
     * @param string|null $cardType
     *
     * @return array
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     * @throws \InvalidArgumentException
     */
    public static function movements(int $boardId, string $period, Carbon $start = null, Carbon $end = null, string $cardType = null): array
    {
        list($start, $end) = self::getRangeLimits($boardId, $start, $end);
        $ranges = TimeRange::getRanges($start, $end, $period);
        $data   = [];
        /** @var Carbon[] $range */
        foreach ($ranges as $range) {
            list($countIn, $countForth, $countBacks, $countOut) = self::countMovementsByRange($range[0], $range[1], $boardId, $cardType);
            $data[] = [
                'date'       => $range[0]->getTimestamp(),
                'countIn'    => $countIn,
                'countForth' => $countForth,
                'countBacks' => $countBacks,
                'countOut'   => $countOut
            ];
        }
        return $data;
    }

    /**
     * @param Carbon      $start
     * @param Carbon      $end
     * @param int         $boardId
     *
     * @param string|null $cardType
     *
     * @return array
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     */
    private static function countMovementsByRange(Carbon $start, Carbon $end, int $boardId, string $cardType = null): array
    {
        $query = 'SELECT COUNT(cm.*) AS count, ldest.type AS destType, lsource.type AS sourceType FROM CardsMovements cm
                  JOIN Cards c on cm.cardId = c.id
                  JOIN Lists ldest on cm.listDestId = ldest.id
                  LEFT JOIN Lists lsource on cm.listSourceId = lsource.id
                  JOIN Boards b on ldest.boardId = b.id
                  WHERE cm.date >= :start AND cm.date <= :end AND b.id = :boardId ' . (is_null($cardType) ? '' : ' AND c.type = :cardType') . '
                  GROUP BY destType, sourceType
        ';

        /** @var CountMovements[] $results */
        $params = [
            'boardId' => $boardId,
            'start'   => $start,
            'end'     => $end,
        ];
        is_null($cardType) ? null : $params['cardType'] = $cardType;

        $results    = Injector::database()->selectAll($query, CountMovements::class, $params);
        $countForth = 0;
        $countBacks = 0;
        $countIn    = 0;
        $countOut   = 0;
        foreach ($results as $result) {
            if (is_null($result->sourceType)) {
                $countIn += $result->count;
                break;
            }
            switch (self::$movementNature[$result->sourceType][$result->destType]) {
                case self::GO_FORTH:
                    $countForth += $result->count;
                    break;
                case self::GO_OUT:
                    $countOut += $result->count;
                    break;
                case self::GO_BACK:
                default:
                    $countBacks += $result->count;
                    break;
            }
        }
        return [$countIn, $countForth, $countBacks, $countOut];
    }

    /**
     * @param int         $boardId
     * @param string      $period
     * @param Carbon|null $start
     * @param Carbon|null $end
     * @param string|null $cardType
     *
     * @return array
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     * @throws \InvalidArgumentException
     */
    public static function countCards(int $boardId, string $period, Carbon $start = null, Carbon $end = null, string $cardType = null): array
    {
        list($start, $end) = self::getRangeLimits($boardId, $start, $end);
        $ranges = TimeRange::getRanges($start, $end, $period);
        $lists  = KanbanList::getAll($boardId);
        $data   = [];
        /** @var Carbon[] $range */
        foreach ($ranges as $range) {
            $count = [
                'date'  => $range[0]->getTimestamp(),
                'total' => 0,
            ];
            foreach ($lists as $list) {
                $countCards         = self::countCardsByRange($start, $list->boardId, $list->id, $cardType);
                $count[$list->type] = $countCards;
                $count['total']     += $countCards;
            }
            $data[] = $count;
        }
        return $data;
    }

    /**
     * @param Carbon $datetime
     * @param int    $boardId
     * @param string $cardType
     * @param int    $listId
     *
     * @return int
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     */
    private static function countCardsByRange(Carbon $datetime, int $boardId, int $listId, $cardType = null): int
    {
        $query = 'SELECT SUM(IF(listDestId = :listId, 1, -1)) FROM CardsMovements cm 
                  JOIN Cards c on cm.cardId = c.id
                  JOIN Lists ldest on cm.listDestId = ldest.id
                  LEFT JOIN Lists lsource on cm.listSourceId = lsource.id
                  JOIN Boards b on ldest.boardId = b.id
            WHERE cm.date <= :date AND b.id = :boardId AND (ldest.id = :listDestId OR lsource.id = :listSourceId)';

        $query = is_null($cardType) ? $query : ($query . ' AND c.type = :cardType');

        $params = [
            'listId'       => $listId,
            'listDestId'   => $listId,
            'listSourceId' => $listId,
            'boardId'      => $boardId,
            'date'         => $datetime,
        ];
        is_null($cardType) ? null : $params['cardType'] = $cardType;
        $count = Injector::database()->selectValue(
            $query,
            $params
        );
        return intval($count);
    }
}
