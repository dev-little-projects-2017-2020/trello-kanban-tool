<?php

namespace Infomaniak\TrelloKanban\Features;

use Infomaniak\TrelloKanban\Models\CardModel;
use Infomaniak\TrelloKanban\Models\ListModel;
use Infomaniak\TrelloKanban\Tools\Injector;
use Infomaniak\TrelloKanban\Trello\BoardManager;
use Infomaniak\TrelloKanban\Trello\ListManager;
use Infomaniak\TrelloKanban\Trello\TrelloRequest;

/**
 * Class KanbanList
 *
 * @package Infomaniak\TrelloKanban\Features
 */
class KanbanList
{
    const IDEAS      = 'ideas';
    const PROJECTS   = 'projects';
    const TODO       = 'todo';
    const ON_GOING   = 'ongoing';
    const VALIDATING = 'validating';
    const DONE       = 'done';
    const DEBT       = 'debt';

    const IDEAS_NAME      = 'Ideas';
    const PROJECTS_NAME   = 'Projects';
    const TODO_NAME       = 'To Do';
    const ON_GOING_NAME   = 'On Going';
    const VALIDATING_NAME = 'Validating';
    const DONE_NAME       = 'Done';
    const DEBT_NAME       = 'Debt';

    public static $types = [
        KanbanList::IDEAS,
        KanbanList::PROJECTS,
        KanbanList::TODO,
        KanbanList::ON_GOING,
        KanbanList::VALIDATING,
        KanbanList::DONE,
        KanbanList::DEBT,
    ];

    private static $maximumByList = [
        KanbanList::IDEAS      => 3,
        KanbanList::PROJECTS   => 2,
        KanbanList::TODO       => 2,
        KanbanList::ON_GOING   => 1,
        KanbanList::VALIDATING => 1,
        KanbanList::DONE       => null,
        KanbanList::DEBT       => null,
    ];

    /**
     * @param $listType
     *
     * @return int|null
     */
    public static function maximumByList($listType)
    {
        return is_null(self::$maximumByList[$listType]) ? null : intval(KanbanBoard::countMembers() * self::$maximumByList[$listType]);
    }

    /**
     * @param BoardManager $boardManager
     *
     * @param              $boardId
     *
     * @return ListModel[]
     */
    public static function createAll(BoardManager $boardManager, $boardId)
    {
        self::create($boardManager, $boardId, self::IDEAS_NAME, self::IDEAS);
        self::create($boardManager, $boardId, self::PROJECTS_NAME, self::PROJECTS);
        self::create($boardManager, $boardId, self::TODO_NAME, self::TODO);
        self::create($boardManager, $boardId, self::ON_GOING_NAME, self::ON_GOING);
        self::create($boardManager, $boardId, self::VALIDATING_NAME, self::VALIDATING);
        self::create($boardManager, $boardId, self::DONE_NAME, self::DONE);
        self::create($boardManager, $boardId, self::DEBT_NAME, self::DEBT);

        return self::getAll($boardManager->id());
    }

    /**
     * @param BoardManager $boardManager
     * @param              $boardId
     * @param              $name
     * @param              $type
     *
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     */
    private static function create(BoardManager $boardManager, $boardId, $name, $type): void
    {
        $list = $boardManager->createList($name)->exec();
        self::initialize($boardId, $type, $list);
    }

    /**
     * @param       $boardId
     * @param       $type
     * @param array $list
     *
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     */
    private static function initialize($boardId, $type, array $list): void
    {
        $query = 'INSERT INTO Lists (listTrelloId, name, boardId, type) VALUES (:listId, :name, :boardId, :type)';
        Injector::database()->insert($query, ['listId' => $list['id'], 'name' => $list['name'], 'boardId' => $boardId, 'type' => $type]);
    }

    /**
     * @param $boardId
     *
     * @return ListModel[]
     */
    public static function getAll($boardId): array
    {
        $query = 'SELECT * FROM Lists WHERE boardId = :boardId';
        return Injector::database()->selectAll($query, ListModel::class, ['boardId' => $boardId]);
    }

    /**
     * @param $listId
     *
     * @return ListModel
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     */
    public static function get($listId): ListModel
    {
        $query = 'SELECT * FROM Lists WHERE id = :listId';
        return Injector::database()->selectOne($query, ListModel::class, ['listId' => $listId]);
    }

    /**
     * @param $listTrelloId
     *
     * @return ListModel
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     */
    public static function getFromTrello($listTrelloId)
    {
        $query = 'SELECT * FROM Lists WHERE listTrelloId = :listTrelloId';
        return Injector::database()->selectOne($query, ListModel::class, ['listTrelloId' => $listTrelloId]);
    }

    /**
     * @param $boardId
     * @param $type
     *
     * @return ListModel
     */
    public static function getByType($boardId, $type): ListModel
    {
        $query = 'SELECT * FROM Lists WHERE boardId = :boardId AND type = :type';
        return Injector::database()->selectOne($query, ListModel::class, ['boardId' => $boardId, 'type' => $type]);
    }

    /**
     * @param string $boardId
     * @param array  $listToType
     *
     * @return ListModel[]
     * @throws \Infomaniak\TrelloKanban\Tools\TrelloKanbanException
     */
    public static function initializeAll(string $boardId, array $listToType)
    {
        foreach (self::$types as $type) {
            self::initialize($boardId, $type, $listToType[$type]);
        }
        return self::getAll($boardId);
    }

    /**
     * @param $lists
     */
    public static function createCardsFromTrello($lists)
    {
        $cardsByList = self::getCardsByLists($lists);
        foreach ($cardsByList as $listId => $trelloCards) {
            $cardsByList[$listId] = array_map(function (array $trelloCard) use ($listId) {
                $cardModel = CardModel::retrieveFromTrello($trelloCard, $listId);
                KanbanCard::createFromTrello($cardModel);
            }, $trelloCards);
        }
    }

    /**
     * @param array $lists
     *
     * @return array
     */
    public static function getCardsByLists(array $lists): array
    {
        /** @var TrelloRequest[] $cardsByList */
        $cardsByList = [];
        foreach ($lists as $list) {
            $cardsByList[$list->id] = (new ListManager($list->listTrelloId))->getCards()->batch();
        }
        Injector::trello()->batchGetClean();
        /** @var array[] $cardsByList */
        $cardsByList = array_map(function (TrelloRequest $request) {
            return $request->response();
        }, $cardsByList);
        return $cardsByList;
    }
}
