<?php

namespace Infomaniak\TrelloKanban\Features;

use Infomaniak\TrelloKanban\Models\LabelModel;
use Infomaniak\TrelloKanban\Tools\Injector;
use Infomaniak\TrelloKanban\Trello\BoardManager;

/**
 * Class KanbanLabel
 *
 * @package Infomaniak\TrelloKanban\Features
 */
class KanbanLabel
{
    const YELLOW = 'yellow';
    const PURPLE = 'purple';
    const BLUE   = 'blue';
    const RED    = 'red';
    const GREEN  = 'green';
    const ORANGE = 'orange';
    const BLACK  = 'black';
    const SKY    = 'sky';
    const PINK   = 'pink';
    const LIME   = 'lime';

    const MAINTENANCE = 'MAINTENANCE';
    const PROJECT     = 'PROJECT';
    const TECH        = 'TECH';

    const BIG_PROJECT  = 'Big Project';
    const PROJECT_TASK = 'Task';

    /**
     * @param BoardManager $boardManager
     * @param              $boardId
     * @param null|array   $colors
     *
     * @return LabelModel[]
     */
    public static function createAll(BoardManager $boardManager, $boardId, $colors = null): array
    {
        $colors = (is_array($colors) && count($colors) === 4) ? $colors : [self::GREEN, self::BLUE, self::SKY, self::PURPLE];
        self::create($boardManager, $boardId, self::MAINTENANCE, $colors[0]);
        self::create($boardManager, $boardId, self::PROJECT, $colors[1]);
        self::create($boardManager, $boardId, self::BIG_PROJECT, $colors[2]);
        self::create($boardManager, $boardId, self::PROJECT_TASK, $colors[2]);
        self::create($boardManager, $boardId, self::TECH, $colors[3]);

        return self::getAll($boardId);
    }

    /**
     * @param BoardManager $boardManager
     * @param              $boardId
     * @param              $name
     * @param              $color
     */
    private static function create(BoardManager $boardManager, $boardId, $name, $color): void
    {
        $label = $boardManager->createLabel($name, $color)->exec();
        $query = 'INSERT INTO Labels (labelTrelloId, boardId, name, color) VALUES (:labelId, :boardId, :name, :color)';
        Injector::database()->insert($query, ['labelId' => $label['id'], 'boardId' => $boardId, 'name' => $name, 'color' => $color]);
    }

    /**
     * @param $boardId
     *
     * @return LabelModel[]
     */
    public static function getAll($boardId): array
    {
        $query = 'SELECT * FROM Labels WHERE boardId = :boardId';
        return Injector::database()->selectAll($query, LabelModel::class, ['boardId' => $boardId]);
    }

    /**
     * @param $boardId
     * @param $type
     *
     * @return LabelModel
     */
    public static function get($boardId, $type): LabelModel
    {
        $query = 'SELECT * FROM Labels WHERE boardId = :boardId AND name = :type';
        return Injector::database()->selectOne($query, LabelModel::class, ['boardId' => $boardId, 'type' => $type]);
    }
}
