<?php

namespace Infomaniak\TrelloKanban\Features;

use Carbon\Carbon;
use Infomaniak\TrelloKanban\Models\CardModel;
use Infomaniak\TrelloKanban\Models\ListModel;
use Infomaniak\TrelloKanban\Models\MemberModel;
use Infomaniak\TrelloKanban\Tools\Injector;
use Infomaniak\TrelloKanban\Tools\TrelloKanbanException;
use Infomaniak\TrelloKanban\Trello\ListManager;

/**
 * Class KanbanCard
 *
 * @package Infomaniak\TrelloKanban\Features
 */
class KanbanCard
{
    const MAINTENANCE  = 'MAINTENANCE';
    const PROJECT      = 'PROJECT';
    const TECH         = 'TECH';
    const PROJECT_TASK = 'PROJECT_TASK';
    public static $typeByLabel             = [
        KanbanLabel::PROJECT_TASK => self::PROJECT_TASK,
        KanbanLabel::PROJECT      => self::PROJECT,
        KanbanLabel::BIG_PROJECT  => self::PROJECT,
        KanbanLabel::MAINTENANCE  => self::MAINTENANCE,
        KanbanLabel::TECH         => self::TECH,
    ];
    private static $allowedInsertionsLabels = [
        KanbanList::IDEAS      => [KanbanLabel::MAINTENANCE, KanbanLabel::BIG_PROJECT, KanbanLabel::PROJECT, KanbanLabel::TECH],
        KanbanList::PROJECTS   => [KanbanLabel::PROJECT_TASK],
        KanbanList::TODO       => [KanbanLabel::MAINTENANCE],
        KanbanList::ON_GOING   => [],
        KanbanList::VALIDATING => [],
        KanbanList::DONE       => [],
        KanbanList::DEBT       => [],
    ];
    private static $additionallabelByType   = [
        KanbanLabel::MAINTENANCE => null,
        KanbanLabel::TECH        => null,
        KanbanLabel::BIG_PROJECT => KanbanLabel::PROJECT,
        KanbanLabel::PROJECT     => KanbanLabel::BIG_PROJECT,
        KanbanLabel::MAINTENANCE => KanbanLabel::PROJECT,
    ];

    /**
     * @param ListModel $listModel
     * @param           $type
     * @param           $name
     * @param           $description
     * @param           $members
     *
     * @return CardModel
     */
    public static function create(ListModel $listModel, $type, $name, $description, $members): CardModel
    {
        if (!in_array($type, self::$allowedInsertionsLabels[$listModel->type])) {
            throw new TrelloKanbanException('entry point not allowed');
        }
        $dueAt = self::dueDateByList($listModel->type);

        $listManager = new ListManager($listModel->listTrelloId);
        $labels      = [KanbanLabel::get($listModel->boardId, $type)->labelTrelloId];
        if (!is_null(self::$additionallabelByType[$type])) {
            $labels[] = KanbanLabel::get($listModel->boardId, self::$additionallabelByType[$type])->labelTrelloId;
        }

        $members = array_map(function (MemberModel $member) {
            return $member->memberTrelloId;
        }, $members);

        $card = $listManager->createCard($name, $description, $dueAt, $labels, $members)->exec();
        return self::insertCard($listModel, $type, $card);
    }

    /**
     * @param $list
     *
     * @return Carbon|null
     */
    private static function dueDateByList($list)
    {
        switch ($list) {
            case KanbanList::IDEAS:
            case KanbanList::TODO:
                return Carbon::now()->addMonth();
            case KanbanList::PROJECTS:
                return Carbon::now()->addMonths(6);
            case KanbanList::ON_GOING:
                return Carbon::now()->addWeeks(2);
            case KanbanList::VALIDATING:
                return Carbon::now()->addDays(3);
            case KanbanList::DONE:
                return null;
            case KanbanList::DEBT:
                return null;
        }
        return null;
    }

    /**
     * @param ListModel $listModel
     * @param           $type
     * @param array     $trelloCard
     *
     * @return CardModel
     * @throws TrelloKanbanException
     */
    private static function insertCard(ListModel $listModel, $type, array $trelloCard): CardModel
    {
        $query  = 'INSERT INTO Cards (cardTrelloId, listId, description, dueAt, completedAT, type, name, link) VALUES (:cardTrelloId, :listId, :description, :dueAt, null, :type, :name, :link)';
        $cardId = Injector::database()->insert($query, [
            'cardTrelloId' => $trelloCard['id'],
            'listId'       => $listModel->id,
            'description'  => $trelloCard['desc'],
            'dueAt'        => self::dueDateByList($listModel->type),
            'type'         => self::$typeByLabel[$type],
            'name'         => substr($trelloCard['name'], 0, 200),
            'link'         => $trelloCard['shortUrl'],
        ]);
        $query  = 'INSERT INTO CardsMovements (cardId, listSourceId, listDestId, date) VALUES (:cardId, NULL, :listDestId, :date)';
        Injector::database()->insert($query, ['date' => Carbon::now(), 'listDestId' => $listModel->id, 'cardId' => $cardId]);

        return self::get($cardId);
    }

    /**
     * @param $cardId
     *
     * @return CardModel
     */
    public static function get($cardId): CardModel
    {
        $query = 'SELECT * FROM Cards WHERE id = :cardId';
        $card  = Injector::database()->selectOne($query, CardModel::class, ['cardId' => $cardId]);
        return $card;
    }

    /**
     * @param CardModel $card
     * @param ListModel $newList
     *
     * @param null      $date
     *
     * @return CardModel
     * @throws TrelloKanbanException
     */
    public static function registerMovement(CardModel $card, ListModel $newList, $date = null): CardModel
    {
        $currentList = KanbanList::get($card->listId);
        //if (!in_array($newList->type, CardLife::$allowedMovement[$currentList->type])) { //@TODO on error insert it in database }
        $dueAt = self::dueDateByList($newList->type);
        $query = 'UPDATE Cards SET listId = :listId, dueAt = :dueAt WHERE id = :cardId';
        Injector::database()->update($query, ['listId' => $newList->id, 'dueAt' => $dueAt, 'cardId' => $card->id]);

        $query = 'INSERT INTO CardsMovements (cardId, listSourceId, listDestId, date) VALUES (:cardId, :listSourceId, :listDestId, :date)';
        Injector::database()->insert($query, [
            'cardId'       => $card->id,
            'listSourceId' => $currentList->id,
            'listDestId'   => $newList->id,
            'date'         => is_null($date) ? Carbon::now() : $date
        ]);

        return self::get($card->id);
    }

    /**
     * @param $trelloCardId
     *
     * @return CardModel
     * @throws TrelloKanbanException
     */
    public static function getFromTrello($trelloCardId)
    {
        $query = 'SELECT * FROM Cards WHERE cardTrelloId = :cardTrelloId';
        $card  = Injector::database()->selectOne($query, CardModel::class, ['cardTrelloId' => $trelloCardId]);
        return $card;
    }

    /**
     * @param CardModel $cardModel
     *
     * @param null      $date
     *
     * @return CardModel
     * @throws TrelloKanbanException
     */
    public static function createFromTrello(CardModel $cardModel, $date = null)
    {
        $query  = 'INSERT INTO Cards (cardTrelloId, listId, description, dueAt, completedAT, type, name, link) VALUES (:cardTrelloId, :listId, :description, :dueAt, null, :type, :name, :link)';
        $cardId = Injector::database()->insert($query, [
            'cardTrelloId' => $cardModel->cardTrelloId,
            'listId'       => $cardModel->listId,
            'description'  => $cardModel->description,
            'dueAt'        => $cardModel->dueAt,
            'type'         => $cardModel->type,
            'name'         => substr($cardModel->name, 0, 200),
            'link'         => $cardModel->link,
        ]);

        $query = 'INSERT INTO CardsMovements (cardId, listSourceId, listDestId, date) VALUES (:cardId, NULL, :listId, :date)';
        Injector::database()->insert($query, [
            'cardId' => $cardId,
            'listId' => $cardModel->listId,
            'date'   => is_null($date) ? Carbon::now() : $date
        ]);
        return self::get($cardId);
    }
}
