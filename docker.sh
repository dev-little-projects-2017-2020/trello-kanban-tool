#!/usr/bin/env bash

docker build -t local/trello/cli-php7.3:latest docker/cli-php7.3/
docker build -t local/trello/cli-php7.3-xdebug:latest docker/cli-php7.3-xdebug/
docker build -t local/trello/composer-php7.3:latest docker/composer-php7.3/
docker build -t local/trello/apache-php7.3:latest docker/apache-php7.3/
