USE TrelloTestDatabase;

SET FOREIGN_KEY_CHECKS = 0;

drop table if exists Boards;
drop table if exists Cards;
drop table if exists CardsMovements;
drop table if exists Labels;
drop table if exists Lists;
drop table if exists Members;

ALTER DATABASE TrelloTestDatabase
  DEFAULT CHARACTER SET utf8mb4
  DEFAULT COLLATE utf8mb4_unicode_ci;

create table Boards (
  id            int auto_increment
    primary key,
  name          varchar(255) null,
  boardTrelloId varchar(255) null,
  lastUpdatedAt varchar(255) null
);

create index Boards_boardId_index
  on Boards (boardTrelloId);

create table Labels (
  id            int auto_increment
    primary key,
  labelTrelloId varchar(255)                                                                                null,
  boardId       int                                                                                         null,
  name          varchar(255)                                                                                null,
  color         enum ('yellow', 'purple', 'blue', 'red', 'green', 'orange', 'black', 'sky', 'pink', 'lime') null
);

create index Labels_boardId_index
  on Labels (boardId);

create index Labels_labelTrelloId_index
  on Labels (labelTrelloId);

create table Lists (
  id           int auto_increment
    primary key,
  listTrelloId varchar(255)                                                                null,
  name         varchar(255)                                                                null,
  boardId      int                                                                         null,
  type         enum ('ideas', 'projects', 'todo', 'ongoing', 'validating', 'done', 'debt') null,
  constraint Lists_Boards_id_fk
    foreign key (boardId) references Boards (id)
);

create table Cards (
  id              int auto_increment
    primary key,
  cardTrelloId    varchar(255)                                            null,
  listId          int                                                     null,
  description     text                                                    null,
  dueAt           date                                                    null,
  completedAT     date                                                    null,
  type            enum ('MAINTENANCE', 'PROJECT', 'PROJECT_TASK', 'TECH') null,
  name            varchar(255)                                            null,
  link            varchar(255)                                            null,
  estimatedWeight int                                                     null comment 'time in hours',
  realWeight      int                                                     null comment 'time in hours',
  constraint Cards_Lists_id_fk
    foreign key (listId) references Lists (id)
);

create index Cards_cardId_index
  on Cards (cardTrelloId);

create index Cards_listId_index
  on Cards (listId);

create index Cards_type_index
  on Cards (type);

create index Lists_listId_index
  on Lists (listTrelloId);

create table CardsMovements (
  id           int auto_increment
    primary key,
  cardId       int      not null,
  listSourceId int      null,
  listDestId   int      null,
  date         datetime not null,
  constraint CardsMovements_Cards_id_fk
    foreign key (cardId) references Cards (id),
  constraint CardsMovements_Lists_id_fk
    foreign key (listSourceId) references Lists (id),
  constraint CardsMovements_Lists_id_fk_2
    foreign key (listDestId) references Lists (id)
);


create table Members (
  id             int auto_increment
    primary key,
  memberTrelloId varchar(255) null,
  available      int(1)       null,
  fullName       varchar(255) null,
  username       varchar(255) null
);

create index Members_memberId_index
  on Members (memberTrelloId);

SET FOREIGN_KEY_CHECKS = 1;

