<?php

namespace Infomaniak\TrelloKanban;

use Infomaniak\TrelloKanban\Features\KanbanBoard;
use Infomaniak\TrelloKanban\Models\BoardModel;
use Infomaniak\TrelloKanban\Tools\Injector;
use Infomaniak\TrelloKanban\Trello\BoardManager;

/**
 * Class EnvironmentTest
 *
 * @package Infomaniak\TrelloKanban
 */
class BoardTest extends TestCase
{
    public function testCreateANewEmptyBoard()
    {
        self::assertSerialized([
            'id'            => 1,
            'name'          => 'Kanban Unit Testing',
            'boardTrelloId' => $this->board->boardTrelloId,
            'lastUpdatedAt' => $this->board->lastUpdatedAt
        ], KanbanBoard::getBoard($this->board->id));
        self::assertCount(1, KanbanBoard::getBoards());
        KanbanBoard::createBoard('An other unit test');
        $boards = array_map(function (BoardModel $boardModel) {
            return $boardModel->name;
        }, KanbanBoard::getBoards());
        self::assertCount(2, $boards);
        self::assertArray(['Kanban Unit Testing', 'An other unit test'], $boards);

        self::assertEquals(14, Injector::database()->selectValue('SELECT COUNT(*) FROM Lists'));
        self::assertEquals(10, Injector::database()->selectValue('SELECT COUNT(*) FROM Labels'));
        self::assertEquals(2, Injector::database()->selectValue('SELECT COUNT(*) FROM Boards'));
    }

    public function testRetrieveBoardMembers()
    {
        self::assertEquals(3, Injector::database()->selectValue('SELECT COUNT(*) FROM Members'));
        self::assertEquals(3, KanbanBoard::countMembers());
        self::assertSerialized([
            [
                'id'             => '1',
                'memberTrelloId' => '5e229941f4ced1178ce28f0a',
                'fullName'       => 'Cunégonde Jacquet',
                'username'       => 'cunegondejacquet',
            ],
            [
                'id'             => '2',
                'memberTrelloId' => '5e02c5777010512137fc7e1b',
                'fullName'       => 'Céline de Roland P',
                'username'       => 'celinederolandp',
            ],
            [
                'id'             => '3',
                'memberTrelloId' => '5e22989cf49ff97e904e71e0',
                'fullName'       => 'Eugène Tartempion',
                'username'       => 'eugenetartempion',
            ]
        ], KanbanBoard::getMembers());
    }

    public function testRetrieveBoardActions()
    {
        $boardManager = new BoardManager($this->board->boardTrelloId);
        $actions      = $boardManager->getActions()->exec();
        $actions      = array_map(function ($action) {
            return $action['type'];
        }, $actions);
        $expected     = [
            'createList',
            'createList',
            'createList',
            'createList',
            'createList',
            'createList',
            'createList',
            'addToOrganizationBoard',
            'createBoard',
        ];
        self::assertArray($expected, $actions);
    }
}
