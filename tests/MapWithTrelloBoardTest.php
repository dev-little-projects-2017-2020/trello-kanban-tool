<?php

namespace Infomaniak\TrelloKanban;

use Carbon\Carbon;
use Infomaniak\TrelloKanban\Features\Actions;
use Infomaniak\TrelloKanban\Features\KanbanBoard;
use Infomaniak\TrelloKanban\Features\KanbanCard;
use Infomaniak\TrelloKanban\Features\KanbanLabel;
use Infomaniak\TrelloKanban\Features\KanbanList;
use Infomaniak\TrelloKanban\Tools\Injector;
use Infomaniak\TrelloKanban\Trello\BoardManager;
use Infomaniak\TrelloKanban\Trello\CardManager;
use Infomaniak\TrelloKanban\Trello\EnvironmentManager;
use Infomaniak\TrelloKanban\Trello\ListManager;

/**
 * Class InitializeFromTrelloTest
 *
 * @package Infomaniak\TrelloKanban
 */
class MapWithTrelloBoardTest extends TestCase
{
    private $c11;
    private $c12;
    private $c13;
    private $c21;
    private $c22;
    private $c31;
    private $c32;
    private $c33;
    private $c34;
    private $c41;
    private $c51;
    private $c52;
    private $c61;
    private $c62;
    private $c63;
    private $c64;
    private $c65;
    private $c71;
    private $c72;
    private $c73;
    private $c74;
    private $c75;
    private $c76;

    public function testDiscoverBoardsAndListsAvailableOnTrello()
    {
        $this->createBoardIntoTrello();
        $response = json_decode(json_encode(KanbanBoard::getBoardsFromTrello()), true);
        self::assertCount(2, $response['boards']);
        self::assertCount(3, $response['members']);
        self::assertCount(7, $response['types']);
        self::assertEquals(__CLASS__, $response['boards'][0]['name']);
        self::assertEquals('Kanban Unit Testing', $response['boards'][1]['name']);

        $lists = json_decode(json_encode(KanbanBoard::getBoardListsFromTrello($response['boards'][0]['boardTrelloId'])), true);
        self::assertCount(7, $lists['lists']);
    }

    /**
     * @return array
     * @throws Tools\TrelloKanbanException
     */
    private function createBoardIntoTrello(): array
    {
        $manager      = new EnvironmentManager();
        $board        = $manager->createBoard(__CLASS__)->exec();
        $boardManager = new BoardManager($board['id']);

        $l1 = $boardManager->createList('COL1 -- DISCUSS')->exec();
        $l2 = $boardManager->createList('COL2 -- PROJECTS')->exec();
        $l3 = $boardManager->createList('COL3 -- CHOSES A FAIRE')->exec();
        $l4 = $boardManager->createList('COL4 -- CHOSES EN COURS')->exec();
        $l5 = $boardManager->createList('COL5 -- DEMANDE DE VALIDATION')->exec();
        $l6 = $boardManager->createList('COL6 -- TOUT BON')->exec();
        $l7 = $boardManager->createList('COL7 -- BACKLOG')->exec();

        $lm1 = new ListManager($l1['id']);
        $lm2 = new ListManager($l2['id']);
        $lm3 = new ListManager($l3['id']);
        $lm4 = new ListManager($l4['id']);
        $lm5 = new ListManager($l5['id']);
        $lm6 = new ListManager($l6['id']);
        $lm7 = new ListManager($l7['id']);

        $lbl1           = $boardManager->createLabel('Some Label 1', KanbanLabel::PINK)->exec();
        $lbl2           = $boardManager->createLabel('Some Label 2', KanbanLabel::ORANGE)->exec();
        $lblMaintenance = $boardManager->createLabel('MAINTENANCE', KanbanLabel::RED)->exec();
        $lbl4           = $boardManager->createLabel('TEST', KanbanLabel::RED)->exec();
        $lblTech        = $boardManager->createLabel('TECH', KanbanLabel::YELLOW)->exec();

        $members = $manager->getMembers()->exec();

        $this->c11 = $lm1->createCard('COL1 -- CARD1 --', 'Toto 1', null, [$lbl1['id'], $lblMaintenance['id']], [$members[1]['id']])->exec();
        $this->c12 = $lm1->createCard('COL1 -- CARD2 --', 'Toto 2', null, [], [])->exec();
        $this->c13 = $lm1->createCard('COL1 -- CARD3 --', 'Toto 3', null, [$lblMaintenance['id'], $lblTech['id']], [])->exec();

        $this->c21 = $lm2->createCard('COL2 -- CARD1 --', 'Toto 4', null, [$lbl1['id']], [$members[0]['id'], $members[2]['id']])->exec();
        $this->c22 = $lm2->createCard('COL2 -- CARD2 --', 'Toto 5', null, [$lbl1['id']], [])->exec();

        $this->c31 = $lm3->createCard('COL3 -- CARD1 --', 'Toto 6', null, [$lbl2['id']], [])->exec();
        $this->c32 = $lm3->createCard('COL3 -- CARD2 --', 'Toto 7', null, [$lbl2['id']], [])->exec();
        $this->c33 = $lm3->createCard('COL3 -- CARD3 --', 'Toto 8', null, [$lbl2['id']], [])->exec();
        $this->c34 = $lm3->createCard('COL3 -- CARD4 --', 'Toto 9', null, [$lbl2['id']], [])->exec();

        $this->c41 = $lm4->createCard('COL4 -- CARD1 --', 'Toto 10', null, [$lblMaintenance['id']], [])->exec();

        $this->c51 = $lm5->createCard('COL5 -- CARD1 --', 'Toto 11', null, [], [])->exec();
        $this->c52 = $lm5->createCard('COL5 -- CARD2 --', 'Toto 12', null, [], [])->exec();

        $this->c61 = $lm6->createCard('COL6 -- CARD1 --', 'Toto 13', null, [], [])->exec();
        $this->c62 = $lm6->createCard('COL6 -- CARD2 --', 'Toto 14', null, [], [])->exec();
        $this->c63 = $lm6->createCard('COL6 -- CARD3 --', 'Toto 15', null, [], [])->exec();
        $this->c64 = $lm6->createCard('COL6 -- CARD4 --', 'Toto 16', null, [$lbl4['id'], $lbl2['id']], [])->exec();
        $this->c65 = $lm6->createCard('COL6 -- CARD5 --', 'Toto 17', null, [], [])->exec();

        $this->c71 = $lm7->createCard('COL7 -- CARD1 --', 'Toto 18', null, [$lblTech['id']], [])->exec();
        $this->c72 = $lm7->createCard('COL7 -- CARD2 --', 'Toto 19', null, [], [])->exec();
        $this->c73 = $lm7->createCard('COL7 -- CARD3 --', 'Toto 20', null, [$lblTech['id']], [])->exec();
        $this->c74 = $lm7->createCard('COL7 -- CARD4 --', 'Toto 21', null, [], [])->exec();
        $this->c75 = $lm7->createCard('COL7 -- CARD5 --', 'Toto 22', null, [], [])->exec();
        $this->c76 = $lm7->createCard('COL7 -- CARD6 --', 'Toto 23', null, [], [])->exec();

        return array($board, $lm1, $lm2, $lm3, $lm4, $lm5, $lm6, $lm7, $members);
    }

    public function testCreateBoardFromExistingTrelloBoard()
    {
        list($board, $lm1, $lm2, $lm3, $lm4, $lm5, $lm6, $lm7, $members) = $this->createBoardIntoTrello();

        $members    = [$members[0]['id'], $members[2]['id']];
        $listToType = [
            KanbanList::IDEAS      => $lm1->id(),
            KanbanList::PROJECTS   => $lm2->id(),
            KanbanList::TODO       => $lm3->id(),
            KanbanList::ON_GOING   => $lm4->id(),
            KanbanList::VALIDATING => $lm5->id(),
            KanbanList::DONE       => $lm6->id(),
            KanbanList::DEBT       => $lm7->id(),
        ];
        KanbanBoard::createFromTrelloBoard($board['id'], $listToType, $members);

        self::assertEquals(2, Injector::database()->selectValue('SELECT COUNT(*) FROM Boards'));
        self::assertEquals(23, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards'));
        self::assertEquals(23, Injector::database()->selectValue('SELECT COUNT(*) FROM CardsMovements'));
        self::assertEquals(10, Injector::database()->selectValue('SELECT COUNT(*) FROM Labels'));
        self::assertEquals(14, Injector::database()->selectValue('SELECT COUNT(*) FROM Lists'));
        self::assertEquals(5, Injector::database()->selectValue('SELECT COUNT(*) FROM Members WHERE available = 1'));

        //23 cards, partited in columns
        self::assertEquals(3, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 1]));
        self::assertEquals(2, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 2]));
        self::assertEquals(4, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 3]));
        self::assertEquals(1, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 4]));
        self::assertEquals(2, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 5]));
        self::assertEquals(5, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 6]));
        self::assertEquals(6, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 7]));
        self::assertEquals(23, 3 + 2 + 4 + 1 + 2 + 5 + 6);

        //23 cards, parttited in categories
        self::assertEquals(20, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE type = :type', ['type' => KanbanCard::MAINTENANCE]));
        self::assertEquals(0, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE type = :type', ['type' => KanbanCard::PROJECT]));
        self::assertEquals(3, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE type = :type', ['type' => KanbanCard::TECH]));
        self::assertEquals(0, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE type = :type', ['type' => KanbanCard::PROJECT_TASK]));
    }

    public function testUpdateBoardRetrievingLastTrelloActivity()
    {
        list($board, $lm1, $lm2, $lm3, $lm4, $lm5, $lm6, $lm7, $members) = $this->createBoardIntoTrello();

        $members    = [$members[0]['id'], $members[2]['id']];
        $listToType = [
            KanbanList::IDEAS      => $lm1->id(),
            KanbanList::PROJECTS   => $lm2->id(),
            KanbanList::TODO       => $lm3->id(),
            KanbanList::ON_GOING   => $lm4->id(),
            KanbanList::VALIDATING => $lm5->id(),
            KanbanList::DONE       => $lm6->id(),
            KanbanList::DEBT       => $lm7->id(),
        ];
        $boardModel = KanbanBoard::createFromTrelloBoard($board['id'], $listToType, $members);

        //EXECUTE 2 ACTIONS

        //New Card
        $lm1->createCard('COL1 -- CARD4 --', 'Toto 4', null, [], [$members[1]])->exec();

        //Update due date on card
        (new CardManager($this->c12['id']))->updateDueDate(Carbon::now()->addDays(100))->exec();

        //REGISTER 2 ACTIONS
        Actions::handleLastActions($boardModel->id);

        //The new card have been registered
        self::assertEquals(24, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards'));
        self::assertEquals(24, Injector::database()->selectValue('SELECT COUNT(*) FROM CardsMovements'));
        self::assertEquals(4, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 1]));
        self::assertEquals(21, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE type = :type', ['type' => KanbanCard::MAINTENANCE]));
        self::assertEquals(0, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE type = :type', ['type' => KanbanCard::PROJECT]));
        self::assertEquals(3, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE type = :type', ['type' => KanbanCard::TECH]));
        self::assertEquals(0, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE type = :type', ['type' => KanbanCard::PROJECT_TASK]));

        //The date have been updated
        self::assertEquals('2019-10-25', Injector::database()->selectValue(
            'SELECT dueAt FROM Cards WHERE cardTrelloId = :cardTrelloId',
            ['cardTrelloId' => $this->c12['id']]
        ));

        //EXECUTE 2 ACTIONS

        //Move card from one list to another
        (new CardManager($this->c11['id']))->moveToList($lm3->id())->exec();

        //Archive a card (it may be considered done)
        (new CardManager($this->c22['id']))->archive()->exec();

        //REGISTER 2 ACTIONS
        Actions::handleLastActions($boardModel->id);

        //The 2 cards have been moved
        self::assertEquals(26, Injector::database()->selectValue('SELECT COUNT(*) FROM CardsMovements'));
        //One less card in col 1 (gone to col3)
        self::assertEquals(3, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 1]));
        //One less card in col 2 (gone to col6)
        self::assertEquals(1, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 2]));
        //One more card in col 3
        self::assertEquals(5, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 3]));
        //No change on col 4
        self::assertEquals(1, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 4]));
        //No change on col 5
        self::assertEquals(2, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 5]));
        //One more card in col 6
        self::assertEquals(6, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 6]));
        //No change on col 7
        self::assertEquals(6, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 7]));

        //EXECUTE 4 ACTIONS

        //Get back a card from archive
        (new CardManager($this->c22['id']))->unarchive($lm3->id())->exec();

        //Move card outside
        (new CardManager($this->c41['id']))->moveToBoard(
            $this->board->boardTrelloId,
            KanbanList::getByType($this->board->id, KanbanList::DEBT)->listTrelloId
        )->exec();

        //Change list name
        $lm2->rename('COL2 -- NEW PROJECTS')->exec();

        //Update label on card
        (new CardManager($this->c12['id']))->addLabel(KanbanLabel::get($boardModel->id, KanbanLabel::TECH)->labelTrelloId)->exec();

        //REGISTER 2 ACTIONS
        Actions::handleLastActions($boardModel->id);

        //The 2 cards have been moved
        self::assertEquals(28, Injector::database()->selectValue('SELECT COUNT(*) FROM CardsMovements'));

        //No change on col 1
        self::assertEquals(3, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 1]));
        //No change in col 2
        self::assertEquals(1, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 2]));
        //One more card in col 3 (from col6)
        self::assertEquals(6, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 3]));
        //One less card in col 4 (gone to col6)
        self::assertEquals(0, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 4]));
        //No change on col 5
        self::assertEquals(2, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 5]));
        //One more less card in col 6 & One less card in col 6 ==> No change
        self::assertEquals(6, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 6]));
        //No change on col 7
        self::assertEquals(6, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = :listId', ['listId' => 7 + 7]));

        //The list have been renamed
        self::assertEquals(0, Injector::database()->selectValue('SELECT COUNT(*) FROM Lists WHERE name = :name', ['name' => 'COL2 -- PROJECTS']));
        self::assertEquals(1, Injector::database()->selectValue('SELECT COUNT(*) FROM Lists WHERE name = :name', ['name' => 'COL2 -- NEW PROJECTS']));

        //The label have been updated
        //One less card in MAINTENANCE category
        self::assertEquals(20, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE type = :type', ['type' => KanbanCard::MAINTENANCE]));
        //No change in project
        self::assertEquals(0, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE type = :type', ['type' => KanbanCard::PROJECT]));
        //One more card in TECH category
        self::assertEquals(4, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE type = :type', ['type' => KanbanCard::TECH]));
        //No change in project tasks
        self::assertEquals(0, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE type = :type', ['type' => KanbanCard::PROJECT_TASK]));
    }
}
