<?php

namespace Infomaniak\TrelloKanban;

use Infomaniak\TrelloKanban\Tools\Injector;

/**
 * Class CounterTest
 *
 * @package Infomaniak\TrelloKanban
 */
class CounterTest extends TestCase
{
    public function testNotReached()
    {
        $counter = Injector::counter(1, 6, 'testing');
        for ($i = 1; $i < 3; $i++) {
            $this->iteration($counter);
        }
    }

    /**
     * @param Tools\Counter $counter
     *
     * @throws \PHPUnit\Framework\ExpectationFailedException
     * @throws \SebastianBergmann\RecursionContext\InvalidArgumentException
     */
    private function iteration(Tools\Counter $counter): void
    {
        $counter->clean();
        self::assertEquals(0, $counter->value());
        for ($i = 0; $i < 6; $i++) {
            self::assertTrue($counter->incr(), 'reached at iteration ' . $i);
        }
        sleep(1);
        $realStart = intval(microtime(true) * 1000);
        for ($i = 0; $i < 6; $i++) {
            usleep(rand(0, 10) * 1000);
            self::assertTrue($counter->incr());
        }
        self::assertFalse($counter->incr());
        $start = intval(microtime(true) * 1000);
        $counter->wait();
        $end  = intval(microtime(true) * 1000);
        $time = ($end - $start);
        self::assertGreaterThan(1000 - ($start - $realStart), $time);
        self::assertLessThan(1000, $this);
    }
}
