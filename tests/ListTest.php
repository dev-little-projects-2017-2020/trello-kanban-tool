<?php

namespace Infomaniak\TrelloKanban;

use Infomaniak\TrelloKanban\Features\KanbanList;

/**
 * Class ListTest
 *
 * @package Infomaniak\TrelloKanban
 */
class ListTest extends TestCase
{
    public function testMaximum()
    {
        $ideas = KanbanList::get(1);
        self::assertEquals(9, KanbanList::maximumByList($ideas->type));
        $projects = KanbanList::get(2);
        self::assertEquals(6, KanbanList::maximumByList($projects->type));
        $todo = KanbanList::get(3);
        self::assertEquals(6, KanbanList::maximumByList($todo->type));
        $ongoing = KanbanList::get(4);
        self::assertEquals(3, KanbanList::maximumByList($ongoing->type));
        $validating = KanbanList::get(5);
        self::assertEquals(3, KanbanList::maximumByList($validating->type));
        $done = KanbanList::get(6);
        self::assertNull(KanbanList::maximumByList($done->type));
        $debt = KanbanList::get(7);
        self::assertNull(KanbanList::maximumByList($debt->type));
    }
}
