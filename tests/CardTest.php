<?php

namespace Infomaniak\TrelloKanban;

use Infomaniak\TrelloKanban\Features\KanbanBoard;
use Infomaniak\TrelloKanban\Features\KanbanCard;
use Infomaniak\TrelloKanban\Features\KanbanLabel;
use Infomaniak\TrelloKanban\Features\KanbanList;
use Infomaniak\TrelloKanban\Tools\Injector;
use Infomaniak\TrelloKanban\Tools\TrelloKanbanException;

/**
 * Class CardTest
 *
 * @package Infomaniak\TrelloKanban
 */
class CardTest extends TestCase
{
    public function testInsertNewCards()
    {
        $listModel    = KanbanList::getByType($this->board->id, KanbanList::IDEAS);
        $ideaId       = $listModel->id;
        $cardProjectA = KanbanCard::create($listModel, KanbanLabel::BIG_PROJECT, 'UnitTesting Project A', 'This is the project description', [
            KanbanBoard::getMember(1),
            KanbanBoard::getMember(2)
        ]);
        self::assertEquals($cardProjectA->dueAt, '2019-08-17');
        self::assertEquals($cardProjectA->type, 'PROJECT');
        self::assertEquals(1, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards'));
        self::assertEquals(1, Injector::database()->selectValue(
            'SELECT COUNT(*) FROM CardsMovements WHERE listSourceId IS NULL AND listDestId = :ideaId',
            ['ideaId' => $ideaId]
        ));

        $listModel        = KanbanList::getByType($this->board->id, KanbanList::TODO);
        $todoId           = $listModel->id;
        $cardMaintenanceA = KanbanCard::create($listModel, KanbanLabel::MAINTENANCE, 'UnitTesting Maintenance A', 'This is the card description', [
            KanbanBoard::getMember(1),
            KanbanBoard::getMember(2)
        ]);
        self::assertEquals($cardMaintenanceA->dueAt, '2019-08-17');
        self::assertEquals($cardMaintenanceA->type, 'MAINTENANCE');
        self::assertEquals(2, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards'));
        self::assertEquals(1, Injector::database()->selectValue(
            'SELECT COUNT(*) FROM CardsMovements WHERE listSourceId IS NULL AND listDestId = :ideaId',
            ['ideaId' => $ideaId]
        ));
        self::assertEquals(1, Injector::database()->selectValue(
            'SELECT COUNT(*) FROM CardsMovements WHERE listSourceId IS NULL AND listDestId = :todoId',
            ['todoId' => $todoId]
        ));

        $listModel = KanbanList::getByType($this->board->id, KanbanList::DONE);
        try {
            KanbanCard::create($listModel, KanbanLabel::BIG_PROJECT, 'UnitTesting Project B', 'This is the project description', [
                KanbanBoard::getMember(1),
                KanbanBoard::getMember(2)
            ]);
            self::fail('inserting a big project in done list should be forbidden');
        } catch (TrelloKanbanException $exception) {
            self::assertEquals('entry point not allowed', $exception->getMessage());
        }
        self::assertEquals(2, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards'));
        self::assertEquals(2, Injector::database()->selectValue('SELECT COUNT(*) FROM CardsMovements'));

        $listModel = KanbanList::getByType($this->board->id, KanbanList::IDEAS);
        try {
            KanbanCard::create($listModel, KanbanLabel::PROJECT_TASK, 'UnitTesting Project A', 'This is the project description', [
                KanbanBoard::getMember(1),
                KanbanBoard::getMember(2)
            ]);
            self::fail('inserting a project task in ideas list should be forbidden');
        } catch (TrelloKanbanException $exception) {
            self::assertEquals('entry point not allowed', $exception->getMessage());
        }
        self::assertEquals(2, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards'));
        self::assertEquals(2, Injector::database()->selectValue('SELECT COUNT(*) FROM CardsMovements'));
    }

    public function testMoveCards()
    {
        $listModelIdeas   = KanbanList::getByType($this->board->id, KanbanList::IDEAS);
        $listModelTodo    = KanbanList::getByType($this->board->id, KanbanList::TODO);
        $listModelProject = KanbanList::getByType($this->board->id, KanbanList::PROJECTS);
        $listModelDebt    = KanbanList::getByType($this->board->id, KanbanList::DEBT);

        $cardProjectA     = KanbanCard::create($listModelIdeas, KanbanLabel::BIG_PROJECT, 'UnitTesting Project A', 'This is the project description', [
            KanbanBoard::getMember(1),
            KanbanBoard::getMember(2)
        ]);
        $cardMaintenanceA = KanbanCard::create($listModelTodo, KanbanLabel::MAINTENANCE, 'UnitTesting Maintenance A', 'This is the card description', [
            KanbanBoard::getMember(1),
            KanbanBoard::getMember(2)
        ]);

        self::assertEquals(2, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards'));
        self::assertEquals(2, Injector::database()->selectValue('SELECT COUNT(*) FROM CardsMovements'));

        KanbanCard::registerMovement($cardProjectA, $listModelProject);

        self::assertEquals(1, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = 2'));
        self::assertEquals(3, Injector::database()->selectValue('SELECT COUNT(*) FROM CardsMovements'));
        self::assertEquals(1, Injector::database()->selectValue(
            'SELECT COUNT(*) FROM CardsMovements WHERE listSourceId = :ideaId AND listDestId = :projectId',
            ['ideaId' => $listModelIdeas->id, 'projectId' => $listModelProject->id]
        ));

        //@TODO : handle process errors instead of exceptions in this case
        //try {
        //    KanbanCard::registerMovement($cardMaintenanceA, $listModelProject);
        //    self::fail('move maintenance card to projects list should be forbidden');
        //} catch (TrelloKanbanException $exception) {
        //    self::assertEquals('movement is not allowed', $exception->getMessage());
        //}

        self::assertEquals(1, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = 2'));
        self::assertEquals(3, Injector::database()->selectValue('SELECT COUNT(*) FROM CardsMovements'));

        KanbanCard::registerMovement($cardMaintenanceA, $listModelDebt);

        self::assertEquals(1, Injector::database()->selectValue('SELECT COUNT(*) FROM Cards WHERE listId = 7'));
        self::assertEquals(4, Injector::database()->selectValue('SELECT COUNT(*) FROM CardsMovements'));
        self::assertEquals(1, Injector::database()->selectValue(
            'SELECT COUNT(*) FROM CardsMovements WHERE listSourceId = :todoId AND listDestId = :debtId',
            ['todoId' => $listModelTodo->id, 'debtId' => $listModelDebt->id]
        ));
    }
}
