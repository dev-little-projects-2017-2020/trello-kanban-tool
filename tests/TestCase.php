<?php

namespace Infomaniak\TrelloKanban;

use Carbon\Carbon;
use Dotenv\Dotenv;
use Infomaniak\TrelloKanban\Features\KanbanBoard;
use Infomaniak\TrelloKanban\Models\BoardModel;
use Infomaniak\TrelloKanban\Tools\Injector;
use Infomaniak\TrelloKanban\Trello\BoardManager;
use Infomaniak\TrelloKanban\Trello\EnvironmentManager;

/**
 * Class TestCase
 *
 * @package Infomaniak\Invoicing
 */
class TestCase extends \PHPUnit\Framework\TestCase
{

    /** @var BoardModel */
    protected $board;

    /**
     * @param $expected
     * @param $actual
     */
    public static function assertSerialized($expected, $actual)
    {
        $actual   = json_decode(json_encode($actual), true);
        $expected = json_decode(json_encode($expected), true);
        self::assertEquals($expected, $actual);
    }

    /**
     * @param $expected
     * @param $actual
     */
    public static function assertArray($expected, $actual)
    {
        self::assertEquals(json_decode(json_encode($expected), true), json_decode(json_encode($actual), true));
    }

    /**
     * {@inheritdoc}
     *
     */
    public function setUp(): void
    {
        parent::setUp();

        //Load environment values
        $dotenv = Dotenv::createImmutable(__DIR__ . '/..', '.env.test');
        $dotenv->load();

        //Set test date
        Carbon::setTestNow('2019-07-17');
        self::assertTrue(Carbon::hasTestNow());
        self::assertEquals(1563321600, Carbon::now()->getTimestamp());

        //Attente active sur le conteneur mysql ...
        for ($i = 1; $i < 5; $i++) {
            try {
                Injector::database()->getDatabaseName();
                break;
            } catch (\Exception $exception) {
                sleep(5);
            }
        }

        //Check MySQL connection
        if (Injector::database()->getDatabaseName() !== 'TrelloTestDatabase') {
            throw new \Exception('database not well configured');
        }

        //Check Trello connection
        $members = (new EnvironmentManager())->getMembers()->exec();
        if (count($members) !== 3 || $members[0]['id'] !== '5e229941f4ced1178ce28f0a' || $members[1]['id'] !== '5e02c5777010512137fc7e1b'
            || $members[2]['id'] !== '5e22989cf49ff97e904e71e0') {
            throw new \Exception('Trello connection not well configured');
        }

        //Execute MySQL migrations
        Injector::database()->exec(file_get_contents(__DIR__ . '/migration.sql'));

        //Clean Trello boards
        $manager          = new EnvironmentManager();
        $currentBoards    = $manager->getAllBoards()->exec();
        $currentBoardsIds = array_map(function ($board) {
            return $board['id'];
        }, $currentBoards);

        foreach ($currentBoardsIds as $currentBoard) {
            $boardManager = new BoardManager($currentBoard);
            $boardManager->delete()->exec();
        }

        //Create fresh Trello board
        KanbanBoard::createMembersFromTrello();
        $this->board = KanbanBoard::createBoard('Kanban Unit Testing');
    }
}
