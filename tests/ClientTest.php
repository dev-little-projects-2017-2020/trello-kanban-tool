<?php

namespace Infomaniak\TrelloKanban;

use Infomaniak\TrelloKanban\Tools\Injector;
use Infomaniak\TrelloKanban\Tools\TrelloKanbanException;
use Infomaniak\TrelloKanban\Trello\BoardManager;
use Infomaniak\TrelloKanban\Trello\EnvironmentManager;
use Infomaniak\TrelloKanban\Trello\ListManager;

/**
 * Class ClientTest
 *
 * @package Infomaniak\TrelloKanban
 */
class ClientTest extends TestCase
{
    public function testCreateAndDeleteBoard()
    {
        $manager          = new EnvironmentManager();
        $currentBoards    = $manager->getAllBoards()->exec();
        $currentBoardsIds = array_map(function ($board) {
            return $board['id'];
        }, $currentBoards);
        $countBoards      = count($currentBoards);

        $board = $manager->createBoard('MyTestBoard')->exec();
        self::assertEquals($countBoards + 1, count($manager->getAllBoards()->exec()));
        self::assertEquals('MyTestBoard', $board['name']);
        $boardManager = new BoardManager($board['id']);

        $boardManager->delete()->exec();
        $newBoards    = $manager->getAllBoards()->exec();
        $newBoardsIds = array_map(function ($board) {
            return $board['id'];
        }, $currentBoards);
        self::assertEquals($countBoards, count($newBoards));
        self::assertEquals($currentBoardsIds, $newBoardsIds);
    }

    public function testRetrieveCardsByList()
    {
        $listId      = Injector::database()->selectValue('SELECT listTrelloId FROM Lists WHERE id = 2');
        $listManager = new ListManager($listId);
        $listManager->createCard('Card Name 1', 'Toto 1', null, [], [])->exec();
        $listManager->createCard('Card Name 2', 'Toto 2', null, [], [])->exec();
        $cards = $listManager->getCards()->exec();
        self::assertCount(2, $cards);
    }

    public function testRetrieveCardsByListViaBash()
    {
        $listId      = Injector::database()->selectValue('SELECT listTrelloId FROM Lists WHERE id = 2');
        $listManager = new ListManager($listId);
        $listManager->createCard('Card Name 1', 'Toto 1', null, [], [])->exec();
        $listManager->createCard('Card Name 2', 'Toto 2', null, [], [])->exec();
        $cards = $listManager->getCards()->batch();
        Injector::trello()->batchGetClean();
        self::assertCount(2, $cards->response());
    }

    public function testBasicBoardQueries()
    {
        $manager      = new EnvironmentManager();
        $board        = $manager->createBoard(__CLASS__)->exec();
        $boardManager = new BoardManager($board['id']);

        $actual = $this->trySimpleRequest($boardManager);
        self::assertIsArray($actual);

        $actual2 = $this->tryBatchRequest($boardManager);
        self::assertIsArray($actual2);
        self::assertEquals($actual2[0], $actual);

        try {
            $this->tryWrongBatchRequest($manager);
            self::fail('batch a POST request did not fail');
        } catch (TrelloKanbanException $exception) {
            self::assertCount(2, $manager->getAllBoards()->exec());
        }

        try {
            $this->tryNonExistingBoard();
            self::fail('GET on invalid board did not fail');
        } catch (TrelloKanbanException $exception) {
            self::assertCount(2, $manager->getAllBoards()->exec());
        }
    }

    /**
     * @param BoardManager $boardManager
     *
     * @return array
     * @throws Tools\TrelloKanbanException
     */
    private function trySimpleRequest(BoardManager $boardManager): array
    {
        $array = $boardManager->get()->exec();
        return $array;
    }

    /**
     * @param BoardManager $boardManager
     *
     * @return array
     */
    private function tryBatchRequest(BoardManager $boardManager): array
    {
        $board   = $boardManager->get()->batch();
        $actions = $boardManager->getActions()->batch();
        Injector::trello()->batchGetClean();
        return [$board->response(), $actions->response()];
    }

    /**
     * @param EnvironmentManager $manager
     *
     * @return array
     * @throws TrelloKanbanException
     */
    private function tryWrongBatchRequest(EnvironmentManager $manager): array
    {
        $batch = $manager->createBoard(__CLASS__ . 'Wrong')->batch();
        return $batch->exec();
    }

    /**
     * @return array
     * @throws TrelloKanbanException
     */
    private function tryNonExistingBoard(): array
    {
        $boardManager = new BoardManager('nimp');
        $board        = $boardManager->get()->exec();
        return $board;
    }
}
