getBoard = function (id) {
  axios.get('/api/board/' + id).then(function (response) {

    console.log(response, response.data, response.data.board, response.data.lists);
    document.getElementById('currentBoardName').innerText = response.data.board.name;
    let list = '<pre>';
    for (let i = 0; i < 7; i++) {
      list += '[' + response.data.lists[i].countCards + '] ' + response.data.lists[i].name + '\n';
    }
    list += '</pre>';
    document.getElementById('currentBoardLists').innerHTML = list;
  });
};

(function () {
  getBoard(1);
})();
