<?php

namespace Routing;

/**
 * Class Route
 *
 * @package Routing
 */
abstract class Route
{

    const GET     = 'GET';
    const HEAD    = 'HEAD';
    const OPTIONS = 'OPTIONS';
    const POST    = 'POST';
    const PUT     = 'PUT';
    const DELETE  = 'DELETE';
    protected $path;
    protected $requestBody;

    /**
     * Route constructor.
     *
     * @param $path
     * @param $requestBody
     */
    public function __construct($path, $requestBody)
    {

        $this->path        = $path;
        $this->requestBody = $requestBody;
    }

    /**
     * @param $method
     */
    public function resolve($method)
    {

        switch ($method) {
            case self::OPTIONS:
                $this->options();
                break;
            case self::HEAD:
                in_array(self::GET, $this->methods()) ? $this->head() : $this->headNotFound();
                break;
            default:
                in_array($method, $this->methods()) ? $this->respond($this->call($method)) : $this->notFound();
                break;
        }
    }

    /**
     *
     */
    protected function options()
    {

        $methods   = $this->methods();
        $methods[] = self::OPTIONS;
        if (in_array(self::GET, $this->methods())) {
            $methods[] = self::HEAD;
        }
        header('Allow:' . implode(',', $methods));
        return;
    }

    /**
     * @return mixed
     */
    protected abstract function methods();

    /**
     *
     */
    protected function head()
    {

        header('Content-Length:' . strlen(json_encode($this->call(self::GET))));
        return;
    }

    /**
     * @param $method
     *
     * @return mixed
     */
    protected abstract function call($method);

    /**
     *
     */
    protected function headNotFound()
    {

        http_response_code(404);
        return;
    }

    /**
     * @param $value
     */
    protected function respond($value)
    {

        echo json_encode($value);
        return;
    }

    /**
     *
     */
    protected function notFound()
    {

        http_response_code(404);
        echo json_encode(['error' => 'path_not_found']);
        return;
    }
}