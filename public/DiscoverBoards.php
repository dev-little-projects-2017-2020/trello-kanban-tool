<?php

namespace Routing;

use Infomaniak\TrelloKanban\Tools\TrelloKanbanException;

/**
 * Class DiscoverBoards
 *
 * @package Routing
 */
class DiscoverBoards extends Route
{

    /**
     * @return array
     */
    protected function methods()
    {

        return [self::GET];
    }

    /**
     * @param $method
     *
     * @return array
     * @throws TrelloKanbanException
     */
    protected function call($method)
    {

        switch ($method) {
            case self::GET:
                return (\Infomaniak\TrelloKanban\Features\KanbanBoard::getBoardsFromTrello());
                break;
        }
        throw new TrelloKanbanException('not_found');
    }
}