<?php

namespace Routing;

use Infomaniak\TrelloKanban\Tools\TrelloKanbanException;

/**
 * Class NotFound
 *
 * @package Routing
 */
class NotFound extends Route
{

    /**
     * @return array
     */
    protected function methods()
    {

        return [];
    }

    /**
     * @param $method
     *
     * @throws TrelloKanbanException
     */
    protected function call($method)
    {
        throw new TrelloKanbanException('not_found');
    }
}