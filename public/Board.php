<?php

namespace Routing;

use Carbon\Carbon;
use Infomaniak\TrelloKanban\Features\DynamicMetric;
use Infomaniak\TrelloKanban\Features\KanbanBoard;
use Infomaniak\TrelloKanban\Tools\TimeRange;
use Infomaniak\TrelloKanban\Tools\TrelloKanbanException;

/**
 * Class AssociateBoard
 *
 * @package Routing
 */
class Board extends Route
{
    protected $id;

    /**
     * Board constructor.
     *
     * @param      $path
     * @param      $requestBody
     * @param null $id
     */
    public function __construct($path, $requestBody, $id = null)
    {
        parent::__construct($path, $requestBody);
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    protected function methods()
    {
        return is_null($this->id) ? [self::POST] : [self::GET];
    }

    /**
     * @param $method
     *
     * @return mixed
     */
    protected function call($method)
    {
        switch ($method) {
            case self::POST:
                return (KanbanBoard::createFromTrelloBoard(
                    $this->requestBody['boardId'], $this->requestBody['lists'], $this->requestBody['members']));
                break;
            case self::GET:
                return [
                    'board' => KanbanBoard::getBoard($this->id),
                    'lists' => DynamicMetric::countCards($this->id, TimeRange::PERIOD_MONTH, Carbon::now())
                ];
        }
        throw new TrelloKanbanException('not_found');
    }
}