<?php

namespace Routing;

require_once '../vendor/autoload.php';

$method = $_SERVER['REQUEST_METHOD'];
$uri    = $_SERVER['REQUEST_URI'];
$path   = explode('/', parse_url($uri)['path']);
array_shift($path);
$body = json_decode(file_get_contents('php://input'), true);

if ($path[0] === 'api') {
    header('Access-Control-Allow-Methods: HEAD, GET, POST, PUT, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: Content-Type');
    header("Content-type:application/json");

    routeApi($path, $body)->resolve($method);
    exit();
}

/**
 * @param $path
 * @param $body
 *
 * @return Route
 */
function routeApi($path, $body): Route
{

    if ($path[1] === 'discover') {
        if ($path[2] === 'boards') {
            return (new DiscoverBoards($path, $body));
        }
        if ($path[2] && $path[3] === 'lists') {
            return (new DiscoverLists($path, $body, $path[2]));
        }
    }
    if ($path[1] === 'board') {
        if(!isset($path[2]) || is_numeric($path[2])) {
            if(isset($path[3]) && $path[3] == 'movements') {
                return (new BoardActions($path, $body,  $path[2]));
            }
            return (new Board($path, $body, isset($path[2]) ? $path[2] : null));
        }

    }
    return (new NotFound($path, $body));
}

require_once 'index.html';