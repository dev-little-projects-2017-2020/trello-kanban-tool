<?php

namespace Routing;

use Infomaniak\TrelloKanban\Tools\AlphaNum;
use Infomaniak\TrelloKanban\Tools\TrelloKanbanException;

/**
 * Class DiscoverLists
 *
 * @package Routing
 */
class DiscoverLists extends Route
{

    private $boardTrelloId;

    /**
     * DiscoverLists constructor.
     *
     * @param $path
     * @param $requestBody
     * @param $boardTrelloId
     */
    public function __construct($path, $requestBody, $boardTrelloId)
    {

        parent::__construct($path, $requestBody);
        $this->boardTrelloId = AlphaNum::sanitize($boardTrelloId);
    }

    /**
     * @return array
     */
    protected function methods()
    {

        return [self::GET];
    }

    /**
     * @param $method
     *
     * @return array
     * @throws TrelloKanbanException
     */
    protected function call($method)
    {

        switch ($method) {
            case self::GET:
                return \Infomaniak\TrelloKanban\Features\KanbanBoard::getBoardListsFromTrello($this->boardTrelloId);
                break;
        }
        throw new TrelloKanbanException('not_found');
    }
}