<?php
/**
 * Created by PhpStorm.
 * User: celineperso
 * Date: 2020-02-11
 * Time: 05:45
 */

namespace Routing;

use Infomaniak\TrelloKanban\Features\DynamicMetric;

/**
 * Class BoardActions
 *
 * @package Routing
 */
class BoardActions extends Board
{

    /**
     * @return mixed
     */
    protected function methods()
    {
        return [self::GET];
    }

    /**
     * @param $method
     *
     * @return mixed
     */
    protected function call($method)
    {
        return DynamicMetric::listMovements($this->id);
    }
}